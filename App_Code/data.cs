﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

/// <summary>
/// Summary description for data
/// </summary>
public class data
{

    //static private string HCStringConnection = @"server=.\SQLEXPRESS;initial catalog=HEADCOUNT; Trusted_Connection=Yes";
    static private string HCStringConnection = @"Data Source=192.168.176.10, 1433; initial catalog=HEADCOUNT; User ID=CewsDevs; Password=CewsIt01";     ///conexion servidor
    public SqlConnection HCConnection = new SqlConnection(HCStringConnection);

    static private string NursingStringConnection = @"Data Source=192.168.176.10, 1433; initial catalog=Nursing; User ID=CewsDevs; Password=CewsIt01";
    //static private string NursingStringConnection = @"Data Source=.\SQLEXPRESS;initial catalog=nursing; Integrated Security=True";
    public SqlConnection NursingConnection = new SqlConnection(NursingStringConnection);

    static private string DisabilityStringConnection = @"Data Source=192.168.176.10, 1433; initial catalog=Nursing; User ID=CewsDevs; Password=CewsIt01";
    //static private string DisabilityStringConnection = @"Data Source=.\SQLEXPRESS;initial catalog=Incapacidad; Integrated Security=True";
    public SqlConnection DisabilityConnection = new SqlConnection(DisabilityStringConnection);

    //static private string conexion2 = @"Data Source=192.168.144.11, 1433; initial catalog=Lineas; User ID=CEWSKL01; Password=CewsKyungshinLear03";
    //static private string cs = @"Data Source=192.168.223.58, 1433; Initial Catalog=CEWS; User ID=cewsusr; Password=cews01";

    static private string ManagementConection = @"Data Source=192.168.176.10, 1433; initial catalog=system_management; User ID=CewsDevs; Password=CewsIt01";
    //static private string ManagementConection = @"server=.\SQLEXPRESS;initial catalog=system_management; Trusted_Connection=Yes";
    public SqlConnection SM_CONNECTION = new SqlConnection(ManagementConection);

    /* TKMS CHECADORES LTTS */
    //static private string LTTSSTRINGCONNECTION = @"Data Source = 192.168.223.58, 1433; Initial Catalog = CEWS; User ID = cewsusr; Password=cews01";
    static private string LTTSSTRINGCONNECTION = @"Data Source=192.168.223.58, 1433; Initial Catalog = CEWS; User ID = cewsusr; Password=cews01";
    public SqlConnection LTTSCONNECTION = new SqlConnection(LTTSSTRINGCONNECTION);

    /* LTTS Methods */
    public DataTable getDatatablebyQueryLTTS(string query)
    {
        SqlDataAdapter adapter = new SqlDataAdapter();
        DataTable table = new DataTable();
        try
        {
            adapter.SelectCommand = new SqlCommand(@query, LTTSCONNECTION);
            adapter.Fill(table);
            LTTSCONNECTION.Close();
            return table;
        }
        catch (Exception ex)
        {
            string error = ex.ToString();
            return table;
        }
    }

    public string getStringValueLTTS(string Query)
    {
        string daresult = "";
        if (Query != null && Query != "")
        {
            using (SqlConnection con = new SqlConnection(LTTSSTRINGCONNECTION))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(Query);
                    cmd.Connection = con;
                    con.Open();
                    daresult = cmd.ExecuteScalar().ToString();
                }
                catch (Exception ex)
                {
                    daresult = "";
                }
            }
        }
        return daresult;
    }

    public int getIntValueLTTS(string Query)
    {
        int daresult = 0;
        if (Query != null && Query != "")
        {
            using (SqlConnection con = new SqlConnection(LTTSSTRINGCONNECTION))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(Query);
                    cmd.Connection = con;
                    con.Open();
                    daresult = (int)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    daresult = 0;
                }

            }
        }
        return daresult;
    }

    public double getDoubleValueLTTS(string Query)
    {
        double daresult = 0;
        if (Query != null && Query != "")
        {
            using (SqlConnection con = new SqlConnection(LTTSSTRINGCONNECTION))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(Query);
                    cmd.Connection = con;
                    con.Open();
                    daresult = (double)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    daresult = 0;
                }

            }
        }
        return daresult;
    }


    public DataTable getDatatablebyQuerySM(string query)
    {
        SqlDataAdapter adapter = new SqlDataAdapter();
        DataTable table = new DataTable();
        try
        {
            adapter.SelectCommand = new SqlCommand(@query, SM_CONNECTION);
            adapter.Fill(table);
            SM_CONNECTION.Close();
            return table;
        }
        catch (Exception)
        {
            return table;
        }
    }

    public string getStringValueSM(string Query)
    {
        string daresult = "";
        if (Query != null && Query != "")
        {
            using (SqlConnection con = new SqlConnection(ManagementConection))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(Query);
                    cmd.Connection = con;
                    con.Open();
                    daresult = (string)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    daresult = "";
                }
            }
        }
        return daresult;
    }
    public string getStringValueNursing(string Query)
    {
        string daresult = "";
        if (Query != null && Query != "")
        {
            using (SqlConnection con = new SqlConnection(NursingStringConnection))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(Query);
                    cmd.Connection = con;
                    con.Open();
                    daresult = (string)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    daresult = "";
                }
            }
        }
        return daresult;
    }

    public string getStringValueDisability(string Query)
    {
        string daresult = "";
        if (Query != null && Query != "")
        {
            using (SqlConnection con = new SqlConnection(DisabilityStringConnection))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(Query);
                    cmd.Connection = con;
                    con.Open();
                    daresult = (string)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    daresult = "";
                }
            }
        }
        return daresult;
    }

    public string getStringValueHC(string Query)
    {
        string daresult = "";
        if (Query != null && Query != "")
        {
            using (SqlConnection con = new SqlConnection(HCStringConnection))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(Query);
                    cmd.Connection = con;
                    con.Open();
                    daresult = (string)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    daresult = "";
                }

            }
        }
        return daresult;
    }
    public int getIntValueSM(string Query)
    {
        int daresult = 0;
        if (Query != null && Query != "")
        {
            using (SqlConnection con = new SqlConnection(ManagementConection))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(Query);
                    cmd.Connection = con;
                    con.Open();
                    daresult = (int)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    daresult = 0;
                }

            }
        }
        return daresult;
    }
    public int getIntValueNursing(string Query)
    {
        int daresult = 0;
        if (Query != null && Query != "")
        {
            using (SqlConnection con = new SqlConnection(NursingStringConnection))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(Query);
                    cmd.Connection = con;
                    con.Open();
                    daresult = (int)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    daresult = 0;
                }

            }
        }
        return daresult;
    }

    public int getIntValueDisability(string Query)
    {
        int daresult = 0;
        if (Query != null && Query != "")
        {
            using (SqlConnection con = new SqlConnection(DisabilityStringConnection))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(Query);
                    cmd.Connection = con;
                    con.Open();
                    daresult = (int)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    daresult = 0;
                }

            }
        }
        return daresult;
    }


    public int getIntValueHC(string Query)
    {
        int daresult = 0;
        if (Query != null && Query != "")
        {
            using (SqlConnection con = new SqlConnection(HCStringConnection))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(Query);
                    cmd.Connection = con;
                    con.Open();
                    daresult = (int)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    daresult = 0;
                }

            }
        }
        return daresult;
    }
    public DataTable TabsSystem(string query)
    {
        if (SM_CONNECTION.State == System.Data.ConnectionState.Open)
        {
            SM_CONNECTION.Close();
        }
        SM_CONNECTION.Open();
        SqlDataAdapter adapter = new SqlDataAdapter();
        DataTable table = new DataTable();
        try
        {

            adapter.SelectCommand = new SqlCommand(@query, SM_CONNECTION);
            adapter.Fill(table);
            return table;
        }
        catch (Exception)
        {
            return table;
        }
        finally
        {
            SM_CONNECTION.Close();
        }
    }
    public DataTable validateUser(string user)
    {
        String query = "SELECT ua.id_ua, ua.username_ua, ua.password_ua, ua.expired_date_ua, ua.is_online, ua.clock_ua FROM user_access ua WHERE ua.username_ua ='" + user + "' AND ua.is_online='false' AND ua.expired_date_ua > GETDATE() AND ua.user_status = 3 ORDER BY ua.id_ua;";

        //String query = "SELECT ua.id_ua, ua.username_ua, ua.password_ua, ua.user_group_ua, ua.expired_date_ua, ua.is_online, ua.clock_ua FROM user_access ua WHERE ua.username_ua ='" + user + "' AND ua.is_online='false' AND ua.expired_date_ua > GETDATE();";
        //return table;
        if (SM_CONNECTION.State == System.Data.ConnectionState.Open)
        {
            SM_CONNECTION.Close();
        }
        SM_CONNECTION.Open();

        SqlDataAdapter adapter = new SqlDataAdapter();
        DataTable table = new DataTable();
        try
        {

            adapter.SelectCommand = new SqlCommand(query, SM_CONNECTION);
            adapter.Fill(table);
            return table;
        }
        catch (Exception)
        {
            return table;
        }
        finally
        {
            SM_CONNECTION.Close();
        }
    }
    public DataTable getDatatablebyQueryHC(string query)
    {
        SqlDataAdapter adapter = new SqlDataAdapter();
        DataTable table = new DataTable();
        try
        {
            adapter.SelectCommand = new SqlCommand(@query, HCConnection);
            adapter.Fill(table);
            HCConnection.Close();
            return table;
        }
        catch (Exception)
        {
            return table;
        }
    }
    public DataTable getDatatablebyQueryNursing(string query)
    {
        SqlDataAdapter adapter = new SqlDataAdapter();
        DataTable table = new DataTable();
        try
        {
            adapter.SelectCommand = new SqlCommand(@query, NursingConnection);
            adapter.Fill(table);
            NursingConnection.Close();
            return table;
        }
        catch (Exception ex)
        {
            return table;
        }
    }

    public DataTable getDatatablebyQueryDisability(string query)
    {
        SqlDataAdapter adapter = new SqlDataAdapter();
        DataTable table = new DataTable();
        try
        {
            adapter.SelectCommand = new SqlCommand(@query, DisabilityConnection);
            adapter.Fill(table);
            NursingConnection.Close();
            return table;
        }
        catch (Exception ex)
        {
            return table;
        }
    }

    public int newInsert(string Query)
    {
        int daresult = 0;
        if (Query != null && Query != "")
        {
            using (SqlConnection con = new SqlConnection(ManagementConection))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(Query);
                    cmd.Connection = con;
                    con.Open();
                    daresult = (int)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    daresult = 0;
                }

            }
        }

        return daresult;
    }
    public int newSystemLog(string what,string userid,string username,string system,string url,string description,string log_type)
    {
        int daresult = 0;
        try
        {
            string Machine_name = Environment.MachineName;
            string Machine_user = Environment.UserName;

            string Query = "INSERT INTO system_logs values(" + userid + ",'" + username + "',SYSDATETIME(),'" + system + "','" + what + "','" + description + "','" + url + "','"+Machine_name+"','"+Machine_user+"','"+log_type+"')";
            //Debug.WriteLine(Query);
            if (Query != null && Query != "")
            {
                using (SqlConnection con = new SqlConnection(ManagementConection))
                {
                    try
                    {
                        SqlCommand cmd = new SqlCommand(Query);
                        cmd.Connection = con;
                        con.Open();
                        daresult = (int)cmd.ExecuteScalar();
                    }
                    catch (Exception ex)
                    {
                        //Debug.WriteLine(ex.ToString());
                        daresult = 0;
                    }

                }
            }
        }catch
        {
            daresult = 0;
        }
        
        return daresult;
    }
}