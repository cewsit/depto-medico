﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Descripción breve de Logs
/// </summary>
public class Logs
{
    data SqlQuery = new data();
    public Logs()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public void SendEmail(string Subject,string userLogged,string system,string url,string content, Exception ex)
    {
        try
        {
            //string AdministratorAccount = "aperezcw@kyungshinlear.com";
            MailAddress SetUpAccount = new MailAddress("appscews@KyungshinLear.onmicrosoft.com");

            MailAddress AdministratorAccount = new MailAddress("aperezcw@kyungshinlear.com");

            MailMessage msj = new MailMessage(SetUpAccount, SetUpAccount);

            msj.CC.Add(AdministratorAccount);

            // Permite añadir archivos al mensaje
            //msj.Attachments.Add(new Attachment(new MemoryStream(bytes), "Test.xlsx"));

            msj.Subject = system +" | "+ Subject;
            string fechahora = DateTime.Now.ToString();
            msj.IsBodyHtml = true;
            string innerBody = "";

            innerBody += "<h3>" + ex.Message + "</h3><br/>";
            innerBody += "<p><b>"+fechahora+"</b> [server time]</p>";
            innerBody += "<p><b>User Logged: </b>"+ userLogged + "</p>";
            innerBody += "<p><b>System: </b>"+ system + "</p>";
            innerBody += "<p><b>URL: </b>"+ url + "</p>";
            innerBody += "<p><b>Description:</b>" + ex.ToString() + "</p>";
            msj.Body = innerBody;
            SmtpClient smtp = new SmtpClient("smtp.office365.com", 587);
            smtp.EnableSsl = true;
            smtp.Credentials = new System.Net.NetworkCredential("appscews@KyungshinLear.onmicrosoft.com", "CewsIT01");
            smtp.Send(msj);
        }
        catch (Exception E)
        {
            Debug.WriteLine(E.ToString());
        }
    }

    public void newError(string what, string userid, string username, string system, string url, Exception ex, string log_type)
    {

        SendEmail("An Error Ocurred",username,system,url,ex.ToString(),ex);
        SqlQuery.newSystemLog(what, userid, username, system, url, ex.Message.Replace("'", ""), log_type);
    }

    public void Email()
    {
        try
        {
            MailAddress AdministratorAccount = new MailAddress("aperezcw@kyungshinlear.com");

            MailMessage msj = new MailMessage("appscews@KyungshinLear.onmicrosoft.com", "aperezcw@kyungshinlear.com");
            //msj.Bcc.Add(CopyAccount);
            //msj.Bcc.Add("mhernandezcw@kyungshinlear.com");
            //msj.Bcc.Add("jpalomocw@kyungshinlear.com");
            msj.CC.Add(AdministratorAccount);
            //msj.Bcc.Add("jose66813@hotmail.com");





            //msj.Attachments.Add(new Attachment(new MemoryStream(bytes), "Test.xlsx"));

            //msj.Subject = "Incidencia de seguridad. ";
            string fechahora = DateTime.Now.ToString();
            msj.IsBodyHtml = true;
            msj.Body = "<p>New Email.</p>";
            SmtpClient smtp = new SmtpClient("smtp.office365.com", 587);
            smtp.EnableSsl = true;

            smtp.Credentials = new System.Net.NetworkCredential("appscews@KyungshinLear.onmicrosoft.com", "CewsIT01");
            //smtp.UseDefaultCredentials = true;
            smtp.Send(msj);


        }
        catch (Exception E)
        {
            E.ToString();
        }
    }
}