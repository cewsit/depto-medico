﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="incapacidades.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <%--ESTE FORMULARIO ES SOLO DE REGISTRO DE INCAPACIDADES EL CUAL POR EL MOMENTO NO SE UTILIZARA--%>
                
    
    <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="font-weight:bold">REGISTRO DE INCAPACIDADES</div>
                            <div class="panel-body" style="height:365px">
                                <div class="input-group">
                                    <div class="form-group col-md-12">
                                        <asp:Label runat="server" ID="lblArea">Area</asp:Label>
                                        <asp:DropDownList runat="server" ID="dplArea" CssClass="form-control" style="border-radius:15px"></asp:DropDownList>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <asp:Label runat="server" ID="lblStartDate">Fecha inicio</asp:Label>
                                        <asp:TextBox runat="server" ID="txtStartDate" CssClass="form-control" style="border-radius:15px"></asp:TextBox>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <asp:Label runat="server" ID="lblEndingDate">Fecha fin</asp:Label>
                                        <asp:TextBox runat="server" ID="txtEndingDate" CssClass="form-control" style="border-radius:15px"></asp:TextBox>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <asp:Label runat="server" ID="lblReason">Motivo</asp:Label>
                                        <asp:DropDownList runat="server" ID="dplReason" CssClass="form-control" style="border-radius:15px"></asp:DropDownList>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <asp:Label runat="server" ID="lblDiagnostic">Diagnostico</asp:Label>
                                        <asp:TextBox runat="server" ID="txtDiagnostic" CssClass="form-control" style="border-radius:15px"></asp:TextBox>
                                    </div>
                                    <div class="col-md-8 col-md-offset-3">
                                        <br />
<%--                                        <asp:Button runat="server" ID="btnSaveMedicalDisability" CssClass="col-md-4 col-md-offset-2 btn btn-default" style="border-radius:15px;background-color:#3c8dbc;color:white" Text="Guardar" />--%>
                                    </div>
                            </div>
                        </div>
                     </div>
                </div>

</asp:Content>
