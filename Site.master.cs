﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Site : System.Web.UI.MasterPage
{
    data sqlquery=new data();
    int SystemID = 11;
    protected void Page_Load(object sender, EventArgs e)
    {
        idbody.Attributes.Add("class", "sidebar-mini skin-blue-light sidebar-collapse");
        try
        {
            if (Session["is_online"] != null)
            {
                if (Session["userid"] != null)
                {
                    int TotalTabs = loadBar(Session["userid"].ToString(), Session["username"].ToString(), "medical department", SystemID); // maintenance_mgm is ID 10 in DB

                    if(TotalTabs > 0)
                    {
                        
                    }
                    idbody.Attributes.Add("class", "sidebar-mini skin-blue-light sidebar-collapse"); // Se expande el panel de pestañas 
                    if (Session["clock"] != null)
                    {
                        ComposeUserInformation();
                    }
                }
            }
            else
            {
                //string currentPage = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
                //if (currentPage.ToLower() != "login.aspx")
                //    Response.Redirect("login.aspx?lasturl="+ currentPage);
            }
        }
        catch (Exception)
        {

        }

    }
    protected void ComposeUserInformation()
    {
        lblUserClock.InnerHtml ="<span class='label bg-gray'>" + Session["clock"].ToString() +"</span>";
        string Query = "SELECT Nombre,ingreso,puesto,depto FROM HEADCOUNT WHERE numreloj='" + Session["clock"].ToString() +"';";
         DataTable table = sqlquery.getDatatablebyQueryHC(Query);
        foreach(DataRow row in table.Rows)
        {
            lclUserInformation.InnerHtml = SentenceCase(row[0].ToString()) + "<br/><small>" + SentenceCase(row[2].ToString()) + "</small><small> Miembro desde "+DateTime.Parse(row[1].ToString()).ToString("MMMM,dd yyyy")+"</small>";
        }
        imgUserProfile.Attributes.Add("src", Session["url_photos"].ToString() + Session["clock"].ToString() + ".jpg"); // Imagen miniatura en navbar
        imgUserProfileMin.Attributes.Add("src", Session["url_photos"].ToString().Trim() + Session["clock"].ToString() + ".jpg"); // Imagen en Panel de Informacion
        lblUser.InnerHtml = Session["username"].ToString() + " - " + sqlquery.getStringValueSM("SELECT ug.name_ug FROM users_group ug LEFT JOIN link_user_system_group lusg on lusg.group_id = ug.id_ug WHERE lusg.system_id = " + SystemID + " AND lusg.user_id = " + Session["userid"].ToString() + " AND lusg.is_approved = 'true';"); ; // se asigna username a elemento Oculto
        lblUserId.InnerHtml = Session["userid"].ToString(); // Se asigna userid a elemento Oculto
        lblAccessRequestButton.InnerHtml = "<a target=\"_blank\" href=\"" + Session["url_sys_mgm"].ToString() + "new_user_request.aspx?systemID="+ SystemID + "&clock=" + Session["clock"].ToString() + "\"><span class=\"label bg-black\">Cambiar Privilegios</span></a>";
        userMenu.Visible = true; // Se muestra el panel de usuario [Superior-Derecha]
    }
    


    protected int loadBar(string userid, string username, string systemName,int systemID)
    {
        string innerString = "<div class=\"user-panel\">" +
                                  "<div class=\"pull-left image\">" +
                                    "<img src=\"/imagenes/avatar.png\" class=\"img-circle\" alt=\"\">" +
                                  "</div>" +
                              "<div class=\"pull-left infolbl\"><p>Welcome, " + username + "</p>" +
                                    "<p><i class=\"fa fa-circle text-success\"></i> Online</p>" +
                              "</div></div>";
        innerString = "<li class='header text-center'>Navegacion Principal</li>";
        string query = "SELECT DISTINCT tp.name_tab_tp, tp.url_tab_tp,tp.icon_span FROM link_user_system_group lusg LEFT JOIN link_user_tab lut ON lut.users_group_lut = lusg.group_id LEFT JOIN tab_profiles tp ON tp.id_tp = lut.tab_profile_lut LEFT JOIN system_profile sp ON sp.id = tp.system_id WHERE lusg.system_id = tp.system_id AND lusg.user_id = " + userid + " AND tp.system_id = " + systemID + " AND tp.is_active='true' AND lusg.is_approved='true' ORDER BY tp.name_tab_tp ASC;";

        //string query = "SELECT tp.name_tab_tp, tp.url_tab_tp,tp.icon_span FROM tab_profiles tp JOIN link_user_tab lut ON tp.id_tp = lut.tab_profile_lut JOIN users_group ug ON lut.users_group_lut = ug.id_ug JOIN user_access ua ON ug.id_ug = ua.user_group_ua JOIN system_profile sp ON sp.id = tp.system_id WHERE ua.id_ua = " + userid + " AND sp.id = "+systemID+" AND tp.is_active='true' ORDER BY tp.name_tab_tp ASC;";
        
        //string query = "SELECT tp.name_tab_tp, tp.url_tab_tp,tp.icon_span FROM tab_profiles tp JOIN link_user_tab lut ON tp.id_tp = lut.tab_profile_lut JOIN users_group ug ON lut.users_group_lut = ug.id_ug JOIN user_access ua ON ug.id_ug = ua.user_group_ua  WHERE ua.id_ua = " + userid + " AND tp.system_tp = '" + systemName + "' AND tp.is_active='true' ORDER BY tp.name_tab_tp ASC;";
        DataTable TabsTable = sqlquery.TabsSystem(query);
        foreach (DataRow row in TabsTable.Rows)
        {
            string name_tab;
            string url_tab;
            string tab_icon;
            if (row[0] != null) { if (row[0].ToString().Trim() == "") { name_tab = "Unnamed"; } else { name_tab = row[0].ToString(); } } else { name_tab = "Unnamed"; }
            if (row[1] != null) { url_tab = row[1].ToString(); } else { url_tab = "#"; }
            if (row[2] != null) { tab_icon = row[2].ToString(); } else { tab_icon = "glyphicon glyphicon-compressed"; }
            innerString += newTabBar(name_tab, url_tab, tab_icon);
        }
        if(TabsTable.Rows.Count <= 0)
        {
            string AddF = "";
            if (Session["clock"] != null)
            {
                AddF = "&clock=" + Session["clock"].ToString();
            }
            innerString = "<li class='header text-center'>No hay funciones <br/> asociadas a tu usuario, <br/>Por favor contacta al administrador.<br/>o<br/><a href='http://192.168.144.10:89/New_User_Request.aspx?systemID="+ SystemID + "" + AddF + "'>Solicita un privilegio de usuario</a></li>";
        }

        //innerString += newTabBar("Cerrar Sesion", "log_out.aspx", "glyphicon glyphicon-log-out");
        sidebarid.InnerHtml = innerString;

        return TabsTable.Rows.Count;
    }
    protected string newTabBar(string name, string url, string icon)
    {
        if (icon == "") { icon = "glyphicon glyphicon-compressed"; }
        return "<li><a href='" + url + "'><i class='" + icon + "'></i><span>" + name + "</span></a></li>";
    }

    //protected string SentenceCase(string Sent)
    //{
    //    var sourcestring = Sent;
    //    // start by converting entire string to lower case
    //    var lowerCase = sourcestring.ToLower();
    //    // matches the first sentence of a string, as well as subsequent sentences
    //    var r = new Regex(@"(^[a-z])|\.\s+(.)", RegexOptions.ExplicitCapture);
    //    // MatchEvaluator delegate defines replacement of setence starts to uppercase
    //    return  r.Replace(lowerCase, s => s.Value.ToUpper());
    //}

    protected string SentenceCase(string Sentence)
    {
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        Sentence = textInfo.ToTitleCase(Sentence.ToLower());
        return Sentence;
    }
}
