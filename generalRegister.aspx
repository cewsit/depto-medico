﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="generalRegister.aspx.cs" Inherits="_generalRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="content">
        <div class="col-md-12">
            <h4 class="box-title" style="font-weight: bold; color: black">Consultas generales</h4>
            <h6>(Busqueda por segmentos de periodo)</h6>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-1 col-md-offset-1" style="text-align:center">
                                <asp:Label ID="lblDateConsultation1" runat="server" style="color:black;text-align:center;border:none;font-weight:bold">Fecha inicial</asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox runat="server" TextMode="Date" ID="txtDateFilter1" CssClass="form-control" style="border-radius:15px;"/>
                            </div>
                            <div class="col-md-1" style="text-align:center">  
                                <asp:Label ID="lblDateConsultation2" runat="server" style="color:black;text-align:center;border:none;font-weight:bold">Fecha final</asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox runat="server" TextMode="Date" ID="txtDateFilter2" CssClass="form-control" style="border-radius:15px;"/>
                            </div>
                            <div class="col-md-1">
                                <asp:Button id="btnFilterConsultation" runat="server" Text="Buscar" style="border-radius:15px;background-color:#3c8dbc;color:white" CssClass="btn btn-default btn-block" OnClick="btnFilterConsultation_Click"/>
                            </div>
                            <div class="col-md-1">
                                <asp:Button ID="btnCloseCoonsultation" runat="server" Text="Salir" style="border-radius:15px;color:black" CssClass="btn btn-default btn-block" OnClick="btnCloseCoonsultation_Click" />
                            </div>
                            <p id="logs" runat="server"></p>
                        </div>
                        <br />
                        <br />
                        <div class="row">
                            <div class="col-md-12" style="height:680px;overflow-x:auto">
                                <asp:GridView runat="server" Visible="true" CssClass="table table-striped table-responsive fordataTable" ID="GdvRegisters" GridLines="None" OnRowDataBound="GdvRegisters_RowDataBound"></asp:GridView>                                
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

