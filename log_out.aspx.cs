﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    data sqlQuery = new data();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["login"] = null;
        //Session["acceso"] = null;
        if(Session["is_online"] != null)
        {
            sqlQuery.newSystemLog("LOGOUT", Session["userid"].ToString(), Session["username"].ToString(), "medical_department", HttpContext.Current.Request.Url.ToString(), "The user is logout correctly", "info");
        }
        
        Session["userid"] = null;
        Session["username"] = null;
        Session["userpassword"] = null;
        Session["usertype"] = null;
        Session["expired_date"] = null;
        Session["is_online"] = null;

        //if (Request.Cookies["username"] != null)
        //{

        //    HttpCookie currentUserCookie = HttpContext.Current.Request.Cookies["username"];
        //    HttpContext.Current.Response.Cookies.Remove("username");
        //    currentUserCookie.Expires = DateTime.Now.AddDays(-10);
        //    currentUserCookie.Value = null;
        //    HttpContext.Current.Response.SetCookie(currentUserCookie);
        //}

        //if (Request.Cookies["password"] != null)
        //{
        //    HttpCookie currentUserCookie = HttpContext.Current.Request.Cookies["password"];
        //    HttpContext.Current.Response.Cookies.Remove("password");
        //    currentUserCookie.Expires = DateTime.Now.AddDays(-10);
        //    currentUserCookie.Value = null;
        //    HttpContext.Current.Response.SetCookie(currentUserCookie);
        //}

        ExpireAllCookies();

        Response.Redirect("Login.aspx");
    }

    private void ExpireAllCookies()
    {
        if (HttpContext.Current != null)
        {
            int cookieCount = HttpContext.Current.Request.Cookies.Count;
            for (var i = 0; i < cookieCount; i++)
            {
                var cookie = HttpContext.Current.Request.Cookies[i];
                if (cookie != null)
                {
                    var expiredCookie = new HttpCookie(cookie.Name)
                    {
                        Expires = DateTime.Now.AddDays(-1),
                        Domain = cookie.Domain
                    };
                    HttpContext.Current.Response.Cookies.Add(expiredCookie); // overwrite it
                }
            }

            // clear cookies server side
            HttpContext.Current.Request.Cookies.Clear();
        }
    }
}