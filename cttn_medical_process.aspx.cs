﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _cttn_medical_process : System.Web.UI.Page
{
    data SqlQuery = new data();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["is_online"] != null)
        {
            if (!IsPostBack)
            {
                txtMetricsDayDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
                getEfficienceByDay();

                getStatus();
                getCurrentDate();
            }

            getMedicalPending();
            getMetrics();
            getEfficienceByDay();
            GetLastProcessed("","0",txtDate.Text);
        }
        else
        {
            Response.Redirect("login.aspx");
        }
    }

    protected void getStatus()
    {
        dplStatus.AppendDataBoundItems = true;
        dplStatus.DataSource = SqlQuery.getDatatablebyQueryHC("SELECT DISTINCT (medical_summary) FROM cttn_medical_test");
        dplStatus.DataTextField = "medical_summary";
        dplStatus.DataValueField = "medical_summary";
        dplStatus.Items.Insert(0, new ListItem("Todos", "0"));
        dplStatus.DataBind();
    }

    protected void getCurrentDate()
    {
        txtDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        ComposeTable();
    }
    protected void ComposeTable()
    {
        if (gdvMedicalPending.Rows.Count > 0)
        {
            gdvMedicalPending.UseAccessibleHeader = true;
            gdvMedicalPending.HeaderRow.TableSection = TableRowSection.TableHeader;
            gdvMedicalPending.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    protected void getMetrics()
    {
        lblWaiting.InnerHtml = SqlQuery.getIntValueHC("SELECT COUNT(*) FROM cttn_recruited WHERE last_status=3 OR last_status=4").ToString();
        lblInRecruitment.InnerHtml = SqlQuery.getIntValueHC("SELECT COUNT(*) FROM cttn_recruited WHERE last_status=1 OR last_status=2").ToString();
        lblProcessed.InnerHtml = SqlQuery.getIntValueHC("SELECT COUNT(*) FROM [dbo].[cttn_medical_test] WHERE CONVERT(date, getdate()) = CONVERT(date, date_add) ").ToString();
    }

    protected void getMedicalPending()
    {
        gdvMedicalPending.DataSource = SqlQuery.getDatatablebyQueryHC("SELECT cr.control_number 'Control', CONCAT( cr.first_name, ' ' ,cr.middle_name, ' ',cr.father_last_name, ' ' ,cr.mother_last_name) 'Nombre Completo',cst.status_name 'Estado',cst.status_style FROM  cttn_recruited cr LEFT JOIN cttn_status_tracking cst ON cst.id = cr.last_status LEFT JOIN cttn_recruitment_source crs ON cr.recruitment_source = crs.id LEFT JOIN shift_profile sp ON shift_number = cr.wish_shift WHERE cr.last_status = 3 OR cr.last_status = 4 ORDER BY cr.insert_date ASC");
        gdvMedicalPending.DataBind();
    }

    protected void gdvMedicalPending_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int status_name = 3;
        int status_style = 4;
        if (e.Row.RowType != DataControlRowType.Header)
        {
            //e.Row.Cells[image_index].Text = "<img class=\"\" style=\"width:17px;margin-top:-5px\" src=\"" + Session["cttn_url_photos"].ToString().Trim() + e.Row.Cells[control_index].Text + ".jpg\"/>";

            e.Row.Cells[status_name].Text = "<span class=\"label\" style=\"" + e.Row.Cells[status_style].Text + "\">" + e.Row.Cells[status_name].Text + "</span>";
        }
        e.Row.Cells[status_style].Visible = false;
    }

    protected void gdvMedicalPending_SelectedIndexChanged(object sender, EventArgs e)
    {

        GridViewRow Selectedrow = gdvMedicalPending.SelectedRow;

        string ControlNumber = (Selectedrow.Cells[1].Text != null ? Selectedrow.Cells[1].Text : "0");

        updateRecruitedTracking(ControlNumber, 4); // Status ID 4 = EN DEPTO MEDICO
        getMedicalPending();
        GetLastProcessed(txtNSS.Text,dplStatus.SelectedValue,txtDate.Text);

        btnApt.Visible = true;
        btnConditionated.Visible = true;
        btnNoApt.Visible = true;

        txtReasons.Text = "";
        txtReasons.Focus();

        DataTable table = SqlQuery.getDatatablebyQueryHC("SELECT CONCAT( cr.first_name, ' ' ,cr.middle_name, ' ',cr.father_last_name, ' ' ,cr.mother_last_name) 'full_name', cr.birthdate, cr.gender, cr.nss, cr.curp, cr.wish_shift, cr.comments, cr.nationality FROM cttn_recruited CR WHERE control_number='" + ControlNumber + "'");
        if (table.Rows.Count > 0)
        {
            MedicalOverlay.Visible = false;
            foreach (DataRow row in table.Rows)
            {
                lblMTName.InnerHtml = row["full_name"].ToString();
                lblMTControlNumber.InnerHtml = ControlNumber;
                lblMTAge.InnerHtml = CalculateAge(row["birthdate"].ToString()).ToString() + " Años";
                lblMTGender.InnerHtml = row["gender"].ToString().ToUpper() == "M" ? "Masculino" : "Femenino";
                lblMTNSS.InnerHtml = row["nss"].ToString();
                lblMTShift.InnerHtml = row["wish_shift"].ToString();
                lblMTCURP.InnerHtml = row["curp"].ToString();
                lblMTNationality.InnerHtml = (row["nationality"].ToString().Trim().ToUpper() == "MEX" ? "Mexicana" : "Extranjera");

                MTRecruitedImage.Attributes.Add("src", Session["cttn_url_photos"].ToString().Trim() + ControlNumber + ".jpg");
            }

            /*already have a medical process?*/

            
        }
        else
        {
            MedicalOverlay.Visible = true;
        }
    }


    protected int CalculateAge(string SourceDate)
    {
        var age = 0;
        try
        {
            DateTime birthdate = DateTime.Parse(SourceDate);
            // Save today's date.
            var today = DateTime.Today;
            // Calculate the age.
            age = today.Year - birthdate.Year;
            // Go back to the year the person was born in case of a leap year
            if (birthdate.Date > today.AddYears(-age)) age--;
        }
        catch (Exception)
        {
            age = 0;
        }


        return age;
    }

    //protected void ValidateCheckBox(object sender, EventArgs e)
    //{
    //    if (cbDoping.Checked)
    //    {
    //        btnApt.Visible = false;
    //        btnConditionated.Visible = false;
    //        btnNoApt.Visible = true;
    //    }
    //    else
    //    {
    //        if(cbGeneral.Checked || cbVisual.Checked || cbObesity.Checked || cbPsychiatric.Checked || cbNotSpecified.Checked)
    //        {
    //            btnNoApt.Visible = true;
    //            btnConditionated.Visible = true;
    //            btnApt.Visible = false;
    //        }
    //        else
    //        {
    //            btnApt.Visible = true;
    //            btnConditionated.Visible = false;
    //            btnNoApt.Visible = false;   
    //        }
    //    }
    //}

    protected void updateRecruitedTracking(string ControlNumber, int statusID)
    {
        int LastStatus = SqlQuery.getIntValueHC("SELECT TOP 1 step_id FROM cttn_recruitment_tracking WHERE recruited_id='" + ControlNumber + "' ORDER BY step_insert_date DESC");
        if (LastStatus != statusID)
        {
            SqlQuery.getDatatablebyQueryHC("UPDATE cttn_recruited SET last_status=" + statusID + ", last_update=SYSDATETIME() WHERE control_number='" + ControlNumber + "' INSERT INTO cttn_recruitment_tracking OUTPUT 1 VALUES ('" + ControlNumber + "','" + Session["userid"].ToString() + "'," + statusID + ",SYSDATETIME())");
        }
    }

    protected void btnApt_Click(object sender, EventArgs e)
    {
        //// Si es apto se ingresa sin ninguna incidencia, solo con observaciones
        //if (txtReasons.Text.Trim() != "")
        //{
        //    string insertQuery = "INSERT INTO cttn_medical_test OUTPUT 1 VALUES('Apto','" + lblMTControlNumber.InnerHtml + "',SYSDATETIME(),'" + txtReasons.Text + "','" + txtComments.Text + "'," + Session["userid"].ToString() + ")";

        //    if (SqlQuery.getIntValueHC(insertQuery) > 0)
        //    {
        //        updateRecruitedTracking(lblMTControlNumber.InnerHtml, 5);
        //        Response.Redirect("cttn_medical_process.aspx");
        //    }
        //}
        //else
        //{
        //    SetAlert("Ingrese Excluyente", "danger");
        //}

        if(txtReasons.Text.Trim() != "")
        {
            ComposeModalConfirm("Apto", txtReasons.Text, txtComments.Text, lblMTControlNumber.InnerHtml);
        }
        else
        {
            SetAlert("Ingrese Excluyente", "danger");
        }
    }

    protected void btnNoApt_Click(object sender, EventArgs e)
    {
        //// Si No es apto se ingresa con las incidencias seleccionadas y las observaciones redactadas
        //if (txtReasons.Text.Trim() != "")
        //{
        //    string insertQuery = "INSERT INTO cttn_medical_test OUTPUT 1 VALUES('No Apto','" + lblMTControlNumber.InnerHtml + "',SYSDATETIME(),'" + txtReasons.Text + "','" + txtComments.Text + "'," + Session["userid"].ToString() + ") UPDATE cttn_recruited SET is_approved='FALSE' OUTPUT 1 WHERE control_number='" + lblMTControlNumber.InnerHtml + "' ";

        //    if (SqlQuery.getDatatablebyQueryHC(insertQuery).Rows.Count > 0)
        //    {
        //        updateRecruitedTracking(lblMTControlNumber.InnerHtml, 7);
        //        Response.Redirect("cttn_medical_process.aspx");
        //    }
        //}
        //else
        //{
        //    SetAlert("Ingrese Excluyente", "danger");
        //}
        if (txtReasons.Text.Trim() != "")
        {
            ComposeModalConfirm("No Apto", txtReasons.Text, txtComments.Text, lblMTControlNumber.InnerHtml);
        }
        else
        {
            SetAlert("Ingrese Excluyente", "danger");
        }

    }

    protected void btnConditionated_Click(object sender, EventArgs e)
    {
        //// Si es Apto Condicionado se ingresa con las incidencias seleccionadas y las observaciones redactadas
        //if (txtReasons.Text.Trim() != "")
        //{
        //    string insertQuery = "INSERT INTO cttn_medical_test OUTPUT 1 VALUES('Apto Condicionado','" + lblMTControlNumber.InnerHtml + "',SYSDATETIME(),'" + txtReasons.Text + "','" + txtComments.Text + "'," + Session["userid"].ToString() + ")";

        //    if (SqlQuery.getIntValueHC(insertQuery) > 0)
        //    {
        //        updateRecruitedTracking(lblMTControlNumber.InnerHtml, 6);
        //        Response.Redirect("cttn_medical_process.aspx");
        //    }
        //}
        //else
        //{
        //    SetAlert("Ingrese Excluyente", "danger");
        //}
        if (txtReasons.Text.Trim() != "")
        {
            ComposeModalConfirm("Apto Condicionado", txtReasons.Text, txtComments.Text, lblMTControlNumber.InnerHtml);
        }
        else
        {
            SetAlert("Ingrese Excluyente", "danger");
        }
    }
    protected void ComposeModalConfirm(string Result, string Reasons, string Comments,string ControlNumber)
    {
        string ResultClass = "img-rounded text-center ";

        if (Result.Trim() == "Apto")
        {
            ResultClass += "bg-green";
        }
        else if (Result.Trim() == "Apto Condicionado")
        {
            ResultClass += "bg-orange";
        }
        else // No Apto
        {
            ResultClass += "bg-red";
        }

        ConfirmResultModal.Attributes.Add("style", "display:block;");

        lblModalResult.InnerHtml = Result;
        lblModalResult.Attributes.Add("class", ResultClass);

        lblModalReasons.InnerHtml = Reasons.Trim() == "0" ? "Sin Excluyentes" : "" + Reasons;
        lblModalComments.InnerHtml = Comments.Trim() == "" ? "Sin Observaciones" : "" + Comments;

        lblModalControlNumber.InnerHtml = ControlNumber;
    }

    protected void SetAlert(string message, string type)
    {
        lblLogs.InnerHtml = message;
        lblLogs.Attributes.Remove("Class");
        lblLogs.Attributes.Add("Class", "label label-" + type);
    }

    protected void getEfficienceByDay()
    {
         string Today = txtMetricsDayDate.Text;
        //string Today = DateTime.Now.AddDays(0).ToString("yyyy-MM-dd");


        //lblEfficienceDayDate.InnerHtml = Today;
        string First_Shift = "'" + Today + " 06:00.001' AND '" + Today + " 16:00.000'";
        string Second_Shift = "'" + Today + " 16:00.001' AND '" + Today + " 00:00.000'";
        string Rows = "";

        DataTable FirstEfficience = SqlQuery.getDatatablebyQueryHC("SELECT CRSource.name,CMedical.medical_summary, COUNT(*) 'total' FROM cttn_medical_test CMedical LEFT JOIN cttn_recruited REC ON REC.control_number = CMedical.control_number LEFT JOIN cttn_recruitment_source CRSource ON CRSource.id = REC.recruitment_source WHERE CMedical.date_add BETWEEN " + First_Shift + " GROUP BY CRSource.name, CMedical.medical_summary ORDER BY CRSource.name, CMedical.medical_summary");

        bool first = true;
        foreach (DataRow row in FirstEfficience.Rows)
        {
            Rows += "<tr>";
            Rows += "   <td> 1 </td>";
            Rows += "   <td>" + row["name"].ToString() + "</td>";
            Rows += "   <td>" + row["medical_summary"].ToString() + "</td>";
            Rows += "   <td>" + row["total"].ToString() + "</td>";
            Rows += "</tr>";
        }

        tblBodyEfficienceByDay.InnerHtml = Rows;
    }

    protected void saveRecruitmentResult(string Result,string Reasons, string Comments, string controlNumber)
    {

        // Si es Apto Condicionado se ingresa con las incidencias seleccionadas y las observaciones redactadas
        if (Reasons.Trim() != "")
        {
            SqlQuery.getIntValueHC("DELETE FROM cttn_recruited_rejection_reason WHERE id = (SELECT MAX(ID) FROM [dbo].[cttn_recruited_rejection_reason] WHERE control_number = '" + controlNumber + "' AND rejection_reason_id = '14')");
            SqlQuery.getIntValueHC("DELETE FROM cttn_medical_test OUTPUT 1 WHERE CONVERT(date,date_add)=CONVERT(date, GETDATE()) AND control_number='"+controlNumber+"' "); // DELETE old status in the day by control number
            SqlQuery.getIntValueHC("DELETE FROM cttn_recruitment_tracking WHERE ID = (SELECT MAX(ID) FROM cttn_recruitment_tracking WHERE recruited_id='" + controlNumber + "' AND (step_id='11' OR step_id='8') AND CONVERT(date, step_insert_date) = CONVERT(date, GETDATE()) ) ");


            string insertQuery = "INSERT INTO cttn_medical_test OUTPUT 1 VALUES('"+Result+"','" + controlNumber + "',SYSDATETIME(),'" + Reasons + "','" + Comments + "'," + Session["userid"].ToString() + ")";

            if (SqlQuery.getIntValueHC(insertQuery) > 0)
            {
                if(Result.Trim() == "No Apto") // Si es rechazado en el examen medico, se hace trigger para mandar a RECHAZADO automaticamente
                {
                    int MedicalTestRejectionID = 14; // ID de Rechazo por Examen Medico

                    if (SqlQuery.getDatatablebyQueryHC("INSERT INTO cttn_recruited_rejection_reason OUTPUT 1 VALUES('" + controlNumber + "'," + MedicalTestRejectionID + ",SYSDATETIME()," + Session["userid"].ToString() + ")  UPDATE cttn_recruited SET is_approved='FALSE', last_status=8, last_update=SYSDATETIME() OUTPUT 1 WHERE control_number='" + controlNumber + "' INSERT INTO cttn_recruitment_tracking OUTPUT 1 VALUES('" + controlNumber + "'," + Session["userid"].ToString() + ",8,SYSDATETIME()) ").Rows.Count > 0)
                    {
                        Response.Redirect("cttn_medical_process.aspx");
                    }
                }
                else // Si es APTO o APTO CONDICIONADO
                {
                    updateRecruitedTracking(controlNumber, 11); // se actualiza a Aceptado
                    Response.Redirect("cttn_medical_process.aspx");
                }
            }
        }
        else
        {
            SetAlert("Ingrese Excluyente", "danger");
        }
    }

    protected void btnModalConfirm_Click(object sender, EventArgs e)
    {
        string Reasons = lblModalReasons.InnerHtml == "Sin Excluyentes" ? "0" : lblModalReasons.InnerHtml;
        string Comments = lblModalComments.InnerHtml == "Sin Observaciones" ? "" : lblModalComments.InnerHtml;

        saveRecruitmentResult(lblModalResult.InnerHtml, Reasons, Comments, lblModalControlNumber.InnerHtml);
    }

    protected void btnModalCancel_Click(object sender, EventArgs e)
    {
        ConfirmResultModal.Attributes.Add("style", "display:none;");
    }

    protected void txtReasons_TextChanged(object sender, EventArgs e)
    {
        btnApt.Visible = true;
        btnConditionated.Visible = true;
        btnNoApt.Visible = true;

        lblLogs.InnerHtml = "";

        if (txtReasons.Text.Trim().Length > 0 && txtReasons.Text.Trim() == "0" )
        {
            btnNoApt.Visible = false;
            btnConditionated.Visible = false;
        }
        else if(txtReasons.Text.Trim().Length > 0)
        {
            btnApt.Visible = false;
        }

        txtComments.Focus();
    }

    protected void GetLastProcessed(string control_number,string status,string date)
    {
        //string Query = "SELECT TOP 5 CR.control_number 'control', CONCAT(CR.first_name,' ', CR.middle_name, ' ', CR.father_last_name,' ', CR.mother_last_name) 'Nombre', CMT.medical_summary 'Resultado', CMT.reasons 'Excluyentes', CMT.comments 'Observaciones' FROM [dbo].[cttn_medical_test] CMT LEFT JOIN cttn_recruited CR ON CR.control_number = CMT.control_number ORDER BY CMT.date_add DESC ;";
                
        gdvLastProcessed.DataSource = SqlQuery.getDatatablebyQueryHC(getQueryResults(control_number,status,date));
        gdvLastProcessed.DataBind();        
    }

    protected string getQueryResults(string control_number, string status, string date)
    {
        string Query = "SELECT CR.control_number 'control', CONCAT(CR.first_name,' ', CR.middle_name, ' ', CR.father_last_name,' ', CR.mother_last_name) 'Nombre', CMT.medical_summary 'Resultado', CMT.reasons 'Excluyentes', CMT.comments 'Observaciones' FROM [dbo].[cttn_medical_test] CMT LEFT JOIN cttn_recruited CR ON CR.control_number = CMT.control_number WHERE ";

        if (control_number.Trim() != "")
        {
            Query += " CR.control_number = '"+control_number.Trim()+"'";
        }
        else
        {
            Query += " CONVERT(DATE,CMT.date_add) = CONVERT(DATE,'" + date + "')";

            if (status.Trim() != "0")
            {
                Query += " AND CMT.medical_summary = '" + status.Trim() + "' ";
            }
        }

        return Query + " ORDER BY CMT.date_add DESC;";
    }

    protected void gdvLastProcessed_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int recruited_name = 2;
        int result_index = 3;
        if (e.Row.RowType != DataControlRowType.Header)
        {
            //e.Row.Cells[image_index].Text = "<img class=\"\" style=\"width:17px;margin-top:-5px\" src=\"" + Session["cttn_url_photos"].ToString().Trim() + e.Row.Cells[control_index].Text + ".jpg\"/>";

            //e.Row.Cells[status_name].Text = "<span class=\"label\" style=\"" + e.Row.Cells[status_style].Text + "\">" + e.Row.Cells[status_name].Text + "</span>";
            e.Row.Cells[recruited_name].Text = SentenceCase(e.Row.Cells[recruited_name].Text);
            string result = e.Row.Cells[result_index].Text.Trim();
            if (result == "Apto")
            {
                e.Row.Cells[result_index].Text = "<span class=\"label bg-green\">"+ result + "</span>";
            }
            else if(result == "No Apto")
            {
                e.Row.Cells[result_index].Text = "<span class=\"label bg-red\">" + result + "</span>";
            }
            else
            {
                e.Row.Cells[result_index].Text = "<span class=\"label bg-orange\">" + result + "</span>";
            }
        }
        //e.Row.Cells[status_style].Visible = false;
    }

    protected void gdvLastProcessed_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow Selectedrow = gdvLastProcessed.SelectedRow;

        string ControlNumber = (Selectedrow.Cells[1].Text != null ? Selectedrow.Cells[1].Text : "0");

        getMedicalPending();

        btnApt.Visible = true;
        btnConditionated.Visible = true;
        btnNoApt.Visible = true;

        txtReasons.Text = "";
        txtReasons.Focus();

        DataTable table = SqlQuery.getDatatablebyQueryHC("SELECT CONCAT( cr.first_name, ' ' ,cr.middle_name, ' ',cr.father_last_name, ' ' ,cr.mother_last_name) 'full_name', cr.birthdate, cr.gender, cr.nss, cr.curp, cr.wish_shift, cr.comments, cr.nationality FROM cttn_recruited CR WHERE control_number='" + ControlNumber + "'");
        if (table.Rows.Count > 0)
        {
            MedicalOverlay.Visible = false;
            foreach (DataRow row in table.Rows)
            {
                lblMTName.InnerHtml = row["full_name"].ToString();
                lblMTControlNumber.InnerHtml = ControlNumber;
                lblMTAge.InnerHtml = CalculateAge(row["birthdate"].ToString()).ToString() + " Años";
                lblMTGender.InnerHtml = row["gender"].ToString().ToUpper() == "M" ? "Masculino" : "Femenino";
                lblMTNSS.InnerHtml = row["nss"].ToString();
                lblMTShift.InnerHtml = row["wish_shift"].ToString();
                lblMTCURP.InnerHtml = row["curp"].ToString();
                lblMTNationality.InnerHtml = (row["nationality"].ToString().Trim().ToUpper() == "MEX" ? "Mexicana" : "Extranjera");

                MTRecruitedImage.Attributes.Add("src", Session["cttn_url_photos"].ToString().Trim() + ControlNumber + ".jpg");
            }
        }
        else
        {
            MedicalOverlay.Visible = true;
        }
    }
    protected string SentenceCase(string Sentence)
    {
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        Sentence = textInfo.ToTitleCase(Sentence.ToLower());
        return Sentence;
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        string control_number = txtNSS.Text;
        string status = dplStatus.SelectedValue;
        string date = txtDate.Text;

        GetLastProcessed(control_number,status,date);
    }
    protected void txtMetricsDayDate_TextChanged(object sender, EventArgs e)
    {
        getEfficienceByDay();
    }
}