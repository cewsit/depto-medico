﻿
$(function () {

    var path = window.location.pathname;
    var page = path.split("/").pop();
    if (page == "Main.aspx") {
        chartConsultations();
    }

    function chartConsultations() {

        // NEW CHART PUGLING
        const labels = JSON.parse($("#MainContent_lblConsultationTags").attr("data"));

        const data = {
            labels: labels,
            datasets: [
                {
                    label: 'Totale de personas',
                    backgroundColor: ["#1738B2", "#A736C8", "#D368A4", "#EE6973"],
                    borderRadius: 10,
                    barThickness: 20,
                    data: JSON.parse($("#MainContent_lblDataConsultation").attr("data")),
                    order: 1
                }
            ]
        };

        const config = {
            type: 'bar',
            data: data,
            responsive: true,
            options: {

            }

        };

        const ProductionChart = new Chart(
        document.getElementById('topConsultations'),
        config
      );
    }


    //--------------
    //- AREA CHART -
    //--------------


    var areaChartData = {
        labels  : ['Abril', 'Mayo', 'Junio', 'Julio'],
        datasets: [
          {
              label               : "Electronics",
              fillColor           : 'rgba(210, 214, 222, 1)',
              strokeColor         : 'rgba(210, 214, 222, 1)',
              pointColor          : 'rgba(210, 214, 222, 1)',
              pointStrokeColor    : '#c1c7d1',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(220,220,220,1)',
              data                : [13, 59, 80, 0]
          },
          {
              label               : 'Consultas',
              fillColor           : 'rgba(60,141,188,0.9)',
              strokeColor         : 'rgba(60,141,188,0.8)',
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(60,141,188,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(60,141,188,1)',
              data                : [28, 48, 40, 0]
          }
        ]
    }

    


    /*
* DONUT CHART
* -----------
*/
    var encodedStr = $("#MainContent_donutData").attr("data"); /// Tomar los datos a utilizar en la grafica, previamente definidos en el DOM por C#

    // Reemplazar todos los UNICODE del texto en caso de tenerlos / DECODIFICAR
    var parser = new DOMParser;
    var dom = parser.parseFromString(
        '<!doctype html><body>' + encodedStr,
        'text/html');
    var decodedString = dom.body.textContent; 
    /// ESTRUCTURA QUE REQUIERE DONUT 
    //var donutData = [
    //  { label: 'Series2', data: 30, color: '#3c8dbc' },
    //  { label: 'Series3', data: 20, color: '#0073b7' },
    //  { label: 'Series4', data: 50, color: '#00c0ef' }
    //]


    decodedString = decodedString.replace(/'/g, "\"") /// REEMPLAZAR todas las comillas simples a commillas dobles
    donutData = JSON.parse(decodedString) //Convertir string a JSON

    $.plot('#donut-chart', donutData, {
        series: {
            pie: {
                show: true,
                radius: 1,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 2 / 3,
                    formatter: labelFormatter,
                    threshold: 0.1
                }
            }
        },
        legend: {
            show: true
        }
    })
    /*
     * END DONUT CHART
     */
    function labelFormatter(label, series) {
        return '<div style="font-size:11px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
          + label
          + '<br>'
          + Math.round(series.percent) + '%</div>'
        console.log(series)
    }

    ////- BAR CHART -
    ////-------------
    //var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
    //var barChart                         = new Chart(barChartCanvas)
    //var barChartData                     = areaChartData
    //barChartData.datasets[1].fillColor   = '#00a65a'
    //barChartData.datasets[1].strokeColor = '#00a65a'
    //barChartData.datasets[1].pointColor  = '#00a65a'
    //var barChartOptions                  = {
    //    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
    //    scaleBeginAtZero        : true,
    //    //Boolean - Whether grid lines are shown across the chart
    //    scaleShowGridLines      : true,
    //    //String - Colour of the grid lines
    //    scaleGridLineColor      : 'rgba(0,0,0,.05)',
    //    //Number - Width of the grid lines
    //    scaleGridLineWidth      : 1,
    //    //Boolean - Whether to show horizontal lines (except X axis)
    //    scaleShowHorizontalLines: true,
    //    //Boolean - Whether to show vertical lines (except Y axis)
    //    scaleShowVerticalLines  : true,
    //    //Boolean - If there is a stroke on each bar
    //    barShowStroke           : true,
    //    //Number - Pixel width of the bar stroke
    //    barStrokeWidth          : 2,
    //    //Number - Spacing between each of the X value sets
    //    barValueSpacing         : 5,
    //    //Number - Spacing between data sets within X values
    //    barDatasetSpacing       : 1,
    //    //String - A legend template
    //    legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
    //    //Boolean - whether to make the chart responsive
    //    responsive              : true,
    //    maintainAspectRatio     : true
    //}

    //barChartOptions.datasetFill = false
    //barChart.Bar(barChartData, barChartOptions)
});