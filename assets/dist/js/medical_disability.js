﻿$(function () {
        
    //if (document.getElementById("MainContent_txtNameDisability").value != "") {
    //    alert()
    //    $('#archivo').click();
    //    alert()
    //}

    $("body").on("keydown", "#MainContent_txtSearchClockNumberBD", function (e) {
        
        if (e.which == 13)
        {
            e.preventDefault();
            document.querySelector("#MainContent_btnSearchClockNumberBD").click();
            
        }
        
    })

    $("body").on("keydown", "#MainContent_txtNameDisability", function (e) {

        if (e.which == 13)            
            e.preventDefault();
    })

    $(document).on('click', '#MainContent_btnNextFirstStep', function () {
        //alert()
        document.getElementById("MainContent_Captura").style.visibility = "";
    });

    $(document).on('click', '#MainContent_A1', function () {
        //alert()
        document.getElementById("MainContent_Archivo").style.visibility = "";
    });

    $(document).on('click', '#MainContent_btnSearchClockNumberBD', function () {
        //alert()
        document.getElementById("MainContent_Captura").style.visibility = "hidden";
        document.getElementById("MainContent_Archivo").style.visibility = "hidden";

    });

    $("body").on("change", ".step2Field", function () {
        var disabilityIDtxt = document.getElementById("MainContent_txtNameDisability")
        var disabilityIDlbl = document.getElementById("MainContent_NumDisabilityStep3")

        var txtStarDateID = document.getElementById("MainContent_txtStarDate")
        var StartDateIDlbl = document.getElementById("MainContent_StartDateStep3")

        var txtFinishDateID = document.getElementById("MainContent_txtFinishDate")
        var DuedateIDlbl = document.getElementById("MainContent_DuedateStep3")      

        var dplDisabilitiesID = $("#MainContent_dplDisabilitiesReasons option:selected").text()
        var diagnosticIDlbl = document.getElementById("MainContent_lblDiagnosticStep3")

        var dplSubsequentID = $("#MainContent_dplSubsequent option:selected").text()
        var lblSubsequent = document.getElementById("MainContent_lblSubsequent")

        disabilityIDlbl.innerHTML = disabilityIDtxt.value;
        StartDateIDlbl.innerHTML = txtStarDateID.value;
        DuedateIDlbl.innerHTML = txtFinishDateID.value;
        diagnosticIDlbl.innerHTML = dplDisabilitiesID;
        lblSubsequent.innerHTML = dplSubsequentID;                       
    })

    $("body").on("change", "#MainContent_FileDisabilities", function () {
        if (document.getElementById("MainContent_FileDisabilities").files.length >= 0) {
            document.getElementById("MainContent_btnSaveDisability").style.visibility = "visible";
        }
    })

    var textbox = document.getElementById("MainContent_txtNameDisability")
    $("body").on("keyup", textbox, function () {
        if(textbox.value != "")
        {
            document.getElementById("MainContent_A1").style.visibility = "visible";
        }
        else
        {
            document.getElementById("MainContent_A1").style.visibility = "hidden";
        }
    })
})

// Pegar Clase step2Field a todos los campos
// Identificar y declarar variables de txt y lbl 
// Asignar txt a lbl


