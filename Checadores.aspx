﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Checadores.aspx.cs" Inherits="_Default" %>

<%--<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>--%>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
  
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <div class="content">
        <br />
        <div class="box box-danger">
            <div class="box-body">
                <div class="form-group">
                    <div class="col-md-2">
                        <div class="col-md-4">
                            <label ID="Label2" class="control-label" for="txtClock_Number">Reloj</label>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtClock_Number" runat="server" class="form-control" ></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-4">
                            <label ID="Label3" class="control-label text-center" for="txtStart_Date" runat="server">
                                Fecha Inicio
                            </label>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox class="form-control text-center" ID="txtStart_Date" runat="server" TextMode="date"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-4">
                            <label ID="Label4" class="control-label text-center" for="txtDue_Date" runat="server">
                                Fecha Final
                            </label>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox class="form-control" ID="txtDue_Date" runat="server" TextMode="date">
                            </asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2"><asp:Button ID="btnFilter" runat="server" class="btn btn-primary btn-block" onclick="Button1_Click" Text="Filtrar" /></div>
                    <div class="col-md-2"><asp:Button ID="Button2" runat="server" class="btn bg-red btn-block" onclick="Button2_Click" Text="Exportar xlsx" /></div>
                </div>
            </div>
        </div>
        <div style="max-height:600px;overflow: auto; ">
             <table id="checkTableID" class="table table-striped" style="width:100%; background-color:white;">
                <thead class="bg-black">
                    <tr>
                        <th width="30%" class="text-center"><span class="fa fa-calendar-check-o"></span> Fecha</th>
                        <th width="25%" class="text-center"><span class="fa  fa-clock-o"></span></span> Hora </th>
                        <th width="20%" class="text-center"><span class="fa fa-barcode"></span> Numero de Reloj</th>
                        <th width="25%" class="text-center"><span class="fa fa-key"></span> ID</th>
                    </tr>
                </thead>
                <tbody id="tbodyCheckTable" runat="server">

                </tbody>
            </table>
        </div>
       
        <p id="lblLogs" runat="server"></p>

    </div>
    <%-- IS the Backend Table --%>
    <%--<div class="" >
    <div style="overflow : auto; width : 100%; max-height : 400px; display:none">
        <asp:GridView ID="GridView1" runat="server" class="table"
            EmptyDataText="No hay incidencias con esa descripcion" GridLines="Vertical" 
            onRowCommand="GridView1_RowCommand" 
            onselectedindexchanged="GridView1_SelectedIndexChanged1">
            <AlternatingRowStyle BackColor="#d9edf7" />
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <HeaderStyle BackColor="#337ab7" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" font-size="15px"/>
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
        </asp:GridView>
    </div>
   </div>--%>
  
</asp:Content>
