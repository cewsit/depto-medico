﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="NewConsultation.aspx.cs" Inherits="_NewConsultation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="content">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-primary" style="height:700px;margin-top:15px">
                <div class="box-header with-border">
                    <h6 class="box-title">Agregar nueva consulta medica</h6>
                </div>
                    <div class="box-body">
                            <div class=" col-md-6 col-md-offset-3">
                                <img class="profile-user-img img-responsive img-circle" src="assets/img/adduser.png" alt="Razor"/>
                           </div>
                            <div class="form-group hidden col-md-6 col-md-offset-3">
                                <label>Fecha</label>
                                <div class="input-group date">
                                     <div class="input-group-addon">
                                         <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                           </div>
                        <div class="form-group col-md-6 col-md-offset-3">
                            <label>Número de reloj</label>
                            <asp:TextBox ID="txtClocknumber" runat="server" CssClass="form-control" style="text-transform:uppercase"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-6 col-md-offset-3">
                            <label>Nombre del empleado</label>
                            <asp:TextBox ID="txtNameEmployee" CssClass="form-control" runat="server" style="text-transform:uppercase"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-6 col-md-offset-3">
                            <label>Sintomas</label>
                            <asp:TextBox ID="txtSymptom" CssClass="form-control" runat="server" Style="text-transform:uppercase"></asp:TextBox>
                       </div>
                        <p id="lblLogsAddRazor" runat="server"></p>
                        <div class="col-md-2 col-md-offset-3">
                            <br />
                            <br />
                            <asp:linkButton ID="btnReturn" CssClass="btn btn-block btn-default" runat="server" style="color:black;background-color:#ECECEC" Text="Cancelar" OnClick="btnReturn_Click"></asp:linkButton>
                        </div>
                        <div class="col-md-2 col-md-push-2">
                            <br />
                            <br />
                            <asp:Button id="btnSave" CssClass="btn btn-block btn-default" runat="server" text="Guardar" style="color:black;background-color:#ECECEC"/>
                        </div>
                </div>
            </div>
        </div>
     </div>

</asp:Content>

