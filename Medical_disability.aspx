﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Medical_disability.aspx.cs" Inherits="_Medical_disability" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="content">
        <div class="row col-md-12">
            <h4 class="box-title" style="font-weight:bold"></h4>
        </div><br />
        <div class="row">
            <div class="col-md-12">
                <div class="" style="background-color:transparent;">
                    <div class="" style="font-weight:bold">METRICOS<span style="font-weight:normal;font-size:10px;"></span>
                        <div class="row " style="margin-top:10px;"> 
                            <div class="col-md-12 ">
                                <div class="col-md-2">
                                    <div class="info-box bg-gray disabled color-palette" style="border-radius: 20px 20px 20px 20px;">
                                        <span class="info-box-icon " style="border-radius: 20px 0px 0px 20px;background-color:white;"><i class="fa fa-stethoscope" style="color:black;"></i></span>
                                        <div class="info-box-content">
                                            <span >Enfermedad General</span>
                                            <span class="info-box-number" id="lblEnfGeneral" runat="server"> - </span>         

                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <div class="info-box bg-gray disabled color-palette" style="border-radius: 20px 20px 20px 20px;">
                                        <span class="info-box-icon" style="border-radius: 20px 0px 0px 20px;background-color:white;"><i class="fa fa-female" style="color:black;"></i></span>
                                        <div class="info-box-content">
                                            <span class="">Maternidad</span>
                                            <span class="info-box-number"> <p id="lblMaternidad" runat="server"> - </p> </span>

                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <div class="info-box bg-gray color-palette" style="border-radius: 20px 20px 20px 20px;">
                                        <span class="info-box-icon" style="border-radius: 20px 0px 0px 20px;background-color:white;"><i class="fa fa-bus" style="color:black;"></i></span>
                                        <div class="info-box-content">
                                            <span>Riesgo Trayecto</span>

                                            <span class="info-box-number"> <p id="lblRgTrayecto" runat="server"> - </p> </span>
                            
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <div class="info-box bg-gray color-palette" style="border-radius: 20px 20px 20px 20px;">
                                        <span class="info-box-icon" style="border-radius: 20px 0px 0px 20px;background-color:white;"><i class="fa fa-building" style="color:black;"></i></span>
                                        <div class="info-box-content">
                                            <span>Riesgo Trabajo</span>                           

                                            <span class="info-box-number"> <p id="lblRgTrabajo" runat="server"> - </p> </span>
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <div class="info-box bg-gray-active color-palette" style="border-radius: 20px 20px 20px 20px;">
                                        <span class="info-box-icon" style="border-radius: 20px 0px 0px 20px;background-color:white;"><img src="assets/img/covid.png" alt="" runat="server" id="img1" width="50" height="50" style="border-radius: 20px 0px 0px 20px;"/></span>
                                        <div class="info-box-content">
                                            <span>COVID</span>
                                            <span class="info-box-number"> <p id="lblCovid" runat="server"> - </p> </span>

                                        </div>
                                    </div>  
                                </div>     
                                <div class="col-md-2">
                                    <div class="panel " style="background-color:transparent;">
                                        <div class="col-md-12 text-center" style="margin-top:-10px">                                                                                        
                                            <asp:TextBox type="month" runat="server" id="txtMetricsDate" name="datefilter" CssClass="form-control txtbox text-center" style="border-radius:0px 0px 0px 0px;margin-top:10px;background-color:white;"/>
                                            
                                            <asp:LinkButton ID="btnMetricsFilter" runat="server" class="btn btn-default  form-control text-blue" OnClick="btnMetricsFilter_Click" style="margin-top:10px;border-radius: 10px 10px 10px 10px;"><i class="fa fa-filter" aria-hidden="true"></i> Filtrado Mensual</asp:LinkButton> 
                                        </div>
                                    </div>
                                </div>                           
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">                
                <div class="panel panel-default">
                    <div class="panel-heading" style="font-weight:bold">INCAPACIDADES
                        <a class="btn btn-app text-blue pull-right" href="Medical_disability.aspx?mode=newDisability" style="border-radius: 10px 10px 10px 10px;"><i class="fa fa-plus-circle"></i>  Nueva Incapacidad</a>
                        <div class="row " style="margin-top:10px;">  
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtDisabilityName" runat="server" AutoPostBack="false" placeholder="Incapacidad" class="form-control" style="border-radius: 10px 10px 10px 10px;" OnTextChanged="txtDisabilityName_TextChanged"></asp:TextBox>
                                    </div>  
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtClockNumber" runat="server" AutoPostBack="false" placeholder="Numero de Reloj" class="form-control" style="border-radius: 10px 10px 10px 10px;" OnTextChanged="txtClockNumber_TextChanged"></asp:TextBox>
                                    </div>  
                                    <div class="col-md-3 text-center" style="margin-top:-20px">
                                        Tipos de Incapacidades
                                        <asp:DropDownList ID="dplDisabilityType" runat="server" AutoPostBack="false" class="form-control text-center" style="border-radius: 10px 10px 10px 10px;" OnSelectedIndexChanged="dplDisabilityType_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                    <div class="col-md-3 text-center" style="margin-top:-20px">
                                        Status de Incapacidades
                                        <asp:DropDownList ID="dplDisabilityStatus" runat="server" AutoPostBack="false" class="form-control text-center" style="border-radius: 10px 10px 10px 10px;" OnSelectedIndexChanged="dplDisabilityStatus_SelectedIndexChanged"></asp:DropDownList>
                                    </div>        
                                </div>
                                <div class="col-md-2 text-center" style="margin-top:-20px">
                                    Fecha Inicio:
                                    <asp:TextBox ID="txtStartDate" type="date" runat="server" AutoPostBack="false" class="form-control text-center" style="border-radius: 10px 10px 10px 10px;" OnSelectedIndexChanged="dplDisabilityStatus_SelectedIndexChanged"></asp:TextBox>
                                </div>

                                <div class="col-md-2 text-center" style="margin-top:-20px">
                                    Fecha Final: 
                                    <asp:TextBox ID="txtDueDate" type="date" runat="server" AutoPostBack="false" class="form-control text-center" style="border-radius: 10px 10px 10px 10px;" OnSelectedIndexChanged="dplDisabilityStatus_SelectedIndexChanged"></asp:TextBox>
                                </div>                   
                             
                                <div class="col-md-1"><asp:LinkButton ID="btnFilter" runat="server" class="btn btn-default text-blue" OnClick="btnFilter_Click" style="border-radius: 10px 10px 10px 10px;"><i class="fa fa-filter" aria-hidden="true"></i> Filtrar</asp:LinkButton> </div>
                                                                
                            </div> 
                        </div>
                    </div>
                    <div class="panel-body">
                        
                        <div class="row">
                                <p id="EmplyTableLog" runat="server"></p>

                            <div class="col-md-12" style="overflow-x:auto">
                                <asp:GridView runat="server" ID="gdvDisability" Visible="true" CssClass="table table-responsive table-striped fordataTable" GridLines="None" OnRowDataBound="gdvDisability_RowDataBound"></asp:GridView>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="mdlhistory" runat="server" tabindex="-1" role="dialog" aria-labelledby="mdlhistory" aria-hidden="true">
        <div class="modal-dialog" style="width:1300px">
            <div class="modal-content" style="width:1300px;margin-right:500px">
                <div class="model-header">
                    <p  class="modal-title" id="mdlhistoryy" style="margin-top:100px;text-align:center"><b>Historial</b></p>
                </div>
                <div class="modal-body" style="text-align:center">
                    <div class="row col-md-12" style="text-align:center">
                        <div class="col-md-2 col-md-offset-4">
                            <img src="assets/img/avatar2.png" class="imgHistoryEmployee" style="width:90px;border:10px;border-color:green;border-radius:15px" id="ImgHistory" runat="server" alt=""/><br /><br />
                        </div>
                        <div class="col-md-3" style="margin-top:25px;">
                            <div class="col-md-12">
                                <asp:Label ID="lblMdlName" runat="server" Visible="true" ></asp:Label>
                            </div>
                            <div class="col-md-12">
                                <asp:label ID="lblMdlClockNumber" runat="server" Visible="true" style="font-weight:bold;margin-top:40px"></asp:label>
                            </div>
                            <div class="col-md-12">
                                <asp:Label ID="lblMdlArea" runat="server" Visible="true" style="margin-top:20px"></asp:Label>
                            </div>
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="col-md-12" style="height:500px;overflow-x:auto">
                            <asp:Gridview runat="server" Visible="true" CssClass="table table-responsive text-center"  id="gdvHistory" GridLines="None" OnRowDataBound="gdvHistory_RowDataBound"></asp:Gridview>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <asp:Button runat="server" ID="btnCloseHistory" CssClass="col-md-2 col-md-offset-5 btn btn-primary" style="border-radius:10px" Text="Salir" OnClick="btnCloseHistory_Click"/>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <%--MODAL ELIMINAR INCAPACIDAD--%>
    <div class="modal fade in bd-example-modal-lg" runat="server" id="mdlDeleteDisability" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h3><b>Eliminar</b></h3>
            </div>
            <div class="modal-body text-center">   
                <h4>¿Desea borrar el siguiente registro?             </h4>
                <a runat="server" ID="lblDisabilityInformation"></a>
            </div>
            <div class="modal-footer">
                <asp:LinkButton type="button" runat="server" id="btnCloseMdlValidate" class="btn btn-secondary" OnClick="btnCloseMdlValidate_Click">Cerrar</asp:LinkButton>
                <asp:LinkButton type="button" runat="server" id="btnDeleteDisability" class="btn btn-primary" OnClick="btnDeleteDisability_Click"> Eliminar </asp:LinkButton>
            </div>
        </div>
        </div>
    </div>

    <%--MODAL INCAPACIDADES--%>
    <div class="modal" id="mdlNewDisabilities" tabindex="-1" role="dialog" runat="server" aria-labelledby="mdlNewDisabilities" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" style="width:1300px">
            <div class="modal-content" style="width:1300px;margin-right:500px;margin-top:160px">
                <div class="col-md-12" style="margin-top:18px">
                    <div class="stepwizard" style="text-align:center">
                        <div class="stepwizard-row setup-panel" style="height:60px;background-color:white; padding-top:7px;">
                            <div class="stepwizard-step col-xs-4" id="Busqueda" runat="server"> 
                                <a href="#step-1" type="button" class="btn btn-success btn-circle" style="font-weight:bold;color:white;" id="busqueda">1</a>
                                <p><small>Búsqueda</small></p>
                            </div>
                            <div class="stepwizard-step col-xs-4" id="Captura" style="visibility:hidden;" runat="server"> 
                                <a href="#step-2" type="button" class="btn btn-default btn-circle" style="font-weight:bold;color:white;" id="captura">2</a>
                                <p><small>Captura</small></p>
                            </div>
                            <div class="stepwizard-step col-xs-4" id="Archivo" style="visibility:hidden;" runat="server"> 
                                <a href="#step-3" type="button" class="btn btn-default btn-circle" style="font-weight:bold;color:white" id="archivo">3</a>
                                <p><small>Archivo</small></p>
                            </div> 
                            <%--<div class="stepwizard-step col-xs-3" id="Vista previa">
                                <a href="#step-4" type="button" class="btn btn-default btn-circle" style="font-weight:bold;color:white" disabled="disabled" id="Vista">4</a>
                                <p><small>Vista previa</small></p>
                            </div>--%>
                        </div>
                    </div>
                    <div class="">
                        <%-- |||||||||||||||||| Start STEP 1 ||||||||||||||||||||||||||||||| --%>
                        <div class="panel panel-default setup-content" id="step-1">                            
                            <div class="panel-heading">
                                <h3 class="panel-title" style="font-weight:bold">Búsqueda de empleado </h3>                                
                            </div>
                            <div class="panel-body">   
                                <p id="stp1Logs" runat="server" class="alert pull-right text-center"></p>
                                                                                                 
                                <div class="row" style="margin-top:30px">
                                    <div class="col-md-offset-2 col-md-2">
                                        <asp:Label runat="server" ID="lblclocknumber" Text="Número de reloj:" style="font-weight:bold" Visible="true">Ingresa numero de  reloj</asp:Label>
                                       
                                        <div class="input-group" style="margin-top:20px">
                                            <span class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </span>
                                            <asp:TextBox name="txtclocknumber" runat="server" class="form-control" id="txtSearchClockNumberBD"></asp:TextBox>
                                        </div>
                                         
                                    </div>
                                    <div class="col-md-4" style="margin-top:40px">
                                        <asp:LinkButton runat="server" ID="btnSearchClockNumberBD" CssClass="btn btn-primary btn-block btn-rounded btn-fw" style="width:120px" OnClick="btnSearchClockNumberBD_Click"> <i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>                                                                            
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-offset-2 col-md-8" style="margin-top:20px;background-color:#E5ECF8; padding-top:30px;padding-bottom:30px;">
                                        <div class="row">
                                             <div class="col-md-4">
                                                <div class="text-center">
                                                    <img src="assets/img/avatar2.png" class="img img-rounded" style="width:100px;margin-top:25px;" id="imageEmployeeBD" runat="server" alt=""/>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row" style="margin-top:20px;">
                                                    <div class="col-md-4">
                                                        <p><asp:Label runat="server" ID="lblClock" style="font-weight:bold"> Número de reloj</asp:Label></p>
                                                       
                                                        <p><asp:Label runat="server" ID="lblName" style="font-weight:bold">Nombre</asp:Label></p>

                                                        <p><asp:Label runat="server" ID="lblArea" style="font-weight:bold">Area</asp:Label></p>

                                                        <p><asp:Label runat="server" ID="lblTurn" style="font-weight:bold">Turno</asp:Label></p>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <p><asp:Label runat="server" ID="txtClockNumberEmployeeBD"></asp:Label></p>

                                                        <p><asp:Label runat="server" ID="txtNameEmployeeBD"></asp:Label></p>

                                                        <p><asp:Label runat="server" ID="txtAreaNameBD"></asp:Label></p>

                                                        <p><asp:Label runat="server" ID="txtTurnEmployeeBD"></asp:Label></p>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="panel-footer" style=" height:50px;">                                
                                <asp:LinkButton ID="btnCloseStp1" CssClass="btn btn-secondary nextBtn pull-left" runat="server" OnClick="btnCloseStp1_Click">Cerrar</asp:LinkButton>
                                <a title="" class="btn btn-primary nextBtn pull-right" href="#" type="button" ID="btnNextFirstStep" runat="server" Visible="false"> Siguiente </a>                                
                            </div>
                        </div>

                        <%-- |||||||||||||||||| Start STEP 2 ||||||||||||||||||||||||||||||| --%>
                        <div class="panel panel-default setup-content" id="step-2" >
                            <div class="panel-heading">
                                <h3 class="panel-title" style="font-weight:bold">Captura de incapacidad</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-2 col-md-8" style="margin-top:20px;background-color:#E5ECF8; padding-top:30px;padding-bottom:30px;">
                                                <div class="row">
                                                     <div class="col-md-12">
                                                        <div class="text-center">
                                                            <img src="assets/img/avatar2.png" class="img img-rounded" style="width:100px;margin-top:25px;" id="imageEmployeeBDstp2" runat="server" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row" style="margin-top:40px">
                                                            <div class="col-md-5">
                                                                <p style="font-weight:bold"> Número de reloj</p>
                                                       
                                                                <p><asp:Label runat="server" ID="lblNameStep2" style="font-weight:bold">Nombre</asp:Label></p>

                                                                <p><asp:Label runat="server" ID="lblAreaStep2" style="font-weight:bold">Area</asp:Label></p>

                                                                <p><asp:Label runat="server" ID="lblShiftStep2" style="font-weight:bold">Turno</asp:Label></p>

                                                            </div>
                                                            <div class="col-md-7">
                                                                <p><asp:Label runat="server" ID="lblClockNumberStp2"> - </asp:Label></p>

                                                                <p><asp:Label runat="server" ID="lblEmployeeName"> - </asp:Label></p>

                                                                <p><asp:Label runat="server" ID="lblEmployeeArea"> - </asp:Label></p>

                                                                <p><asp:Label runat="server" ID="lblShift"> - </asp:Label></p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6"> 
                                        <div class="col-md-10 col-md-offset-1" style="margin-top:15px">
                                            <asp:Label runat="server" ID="lblDisability" Text="Número de incapacidad: " style="font-weight:bold"></asp:Label>
                                            <asp:TextBox runat="server" ID="txtNameDisability" CssClass="form-control step2Field" style="border-radius:15px" AutoPostback="false" OnTextChanged="txtNameDisability_TextChanged"></asp:TextBox>
                                        </div>
                                        <div class="col-md-10 col-md-offset-1" style="margin-top:15px">
                                            <asp:Label runat="server" ID="lblStarDate" Text="Fecha de Inicio:" style="font-weight:bold"></asp:Label>
                                            <asp:TextBox runat="server" ID="txtStarDate" TextMode="Date" CssClass="form-control step2Field" style="border-radius:15px;" AutoPostBack="false"></asp:TextBox>
                                        </div>
                                        <div class="col-md-10 col-md-offset-1" style="margin-top:15px">
                                            <asp:Label runat="server" ID="Label10" Text="Fecha de prueba:" style="font-weight:bold"></asp:Label>
                                            <asp:TextBox runat="server" ID="txtTest" TextMode="Date" CssClass="form-control step2Field" style="border-radius:15px;" AutoPostBack="false"></asp:TextBox>
                                        </div>
                                        <div class="col-md-10 col-md-offset-1" style="margin-top:15px">
                                            <asp:Label runat="server" ID="lblFinishDate" Text="Fecha de Terminación:" style="font-weight:bold"></asp:Label>
                                            <asp:TextBox runat="server" ID="txtFinishDate" TextMode="Date" CssClass="form-control step2Field" style="border-radius:15px" AutoPostBack="false"></asp:TextBox>
                                        </div>
                                        <div class="col-md-10 col-md-offset-1" style="margin-top:15px">
                                            <asp:Label runat="server" ID="lbldiagnostic" Text="Motivo:" style="font-weight:bold"></asp:Label>
                                            <asp:DropDownList runat="server" ID="dplDisabilitiesReasons" CssClass="form-control step2Field" style="border-radius:15px" AutoPostBack="false"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-10 col-md-offset-1" style="margin-top:15px">
                                            <b>Subsecuente:</b>
                                            <asp:CheckBox Text="Subsecuente" runat="server" ID="CbxAppointment" OnCheckedChanged="CbxAppointment_CheckedChanged" AutoPostBack="false" Visible="false"/>
                                            <asp:DropDownList runat="server" ID="dplSubsequent" CssClass="form-control step2Field" style="border-radius:15px" OnSelectedIndexChanged="dplNumberDisability_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-10 col-md-offset-1 hidden" style="margin-top:15px;text-align:center">
                                            
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer" style=" height:50px;">
                                <asp:LinkButton ID="btnCloseStp2" CssClass="btn btn-secondary nextBtn pull-left" runat="server" OnClick="btnCloseStp1_Click">Cerrar</asp:LinkButton>
                                <a title="" class="btn btn-primary nextBtn pull-right" href="#" type="button" ID="A1" runat="server" style="visibility:hidden"> Siguiente </a>
                                
                            </div>
                        </div>

                        <%-- |||||||||||||||||| Start STEP 3 ||||||||||||||||||||||||||||||| --%>
                        <div class="panel panel-default setup-content" id="step-3">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="font-weight:bold">Cargar archivo</h3>
                            </div>
                            <div class="panel-body">
                                <p runat="server" ID="alert" class="alert pull-right text-center"></p>                          

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-2 col-md-8" style="margin-top:20px;background-color:#E5ECF8; padding-top:30px;padding-bottom:30px;">
                                                <div class="row">
                                                     <div class="col-md-12">
                                                        <div class="text-center">
                                                            <img src="assets/img/avatar2.png" class="img img-rounded" style="width:100px;margin-top:25px;" id="imageEmployeeBDstp4" runat="server" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row" style="margin-top:40px">
                                                            <div class="col-md-5">
                                                                <p><asp:Label runat="server"  style="font-weight:bold"> Número de reloj</asp:Label></p>
                                                       
                                                                <p><asp:Label runat="server"  style="font-weight:bold">Nombre</asp:Label></p>

                                                                <p><asp:Label runat="server" style="font-weight:bold">Area</asp:Label></p>

                                                                <p><asp:Label runat="server" style="font-weight:bold">Turno</asp:Label></p>

                                                            </div>
                                                            <div class="col-md-7">
                                                                <p><asp:Label runat="server" ID="lblClockNumberStp4"> - </asp:Label></p>

                                                                <p><asp:Label runat="server" ID="lblEmployeeNameStp4"> - </asp:Label></p>

                                                                <p><asp:Label runat="server" ID="lblDeptoCodeStp4"> - </asp:Label></p>

                                                                <p><asp:Label runat="server" ID="lblShiftStp4"> - </asp:Label></p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-2 col-md-8" style="margin-top:20px; padding-top:30px;padding-bottom:30px">
                                                <div class="col-md-12">
                                                    <asp:Label runat="server" ID="lblNumDisabilityStep3" Text="Número de incapacidad: " style="font-weight:bold"></asp:Label>
                                                    <asp:Label runat="server" ID="NumDisabilityStep3"> - </asp:Label>
                                                </div>
                                                <div class="col-md-12" style="margin-top:15px">
                                                    <asp:Label runat="server" ID="lblStartDateStep3" Text="Fecha de Inicio: " style="font-weight:bold"></asp:Label>
                                                    <asp:Label runat="server" ID="StartDateStep3"> - </asp:Label>
                                                </div>
                                                <div class="col-md-12" style="margin-top:15px">
                                                    <asp:Label runat="server" ID="lblDuedateStep3" Text="Fecha de Terminación: " style="font-weight:bold"></asp:Label>
                                                    <asp:Label runat="server" ID="DuedateStep3"> - </asp:Label>
                                                </div>
                                                <div class="col-md-12" style="margin-top:15px">
                                                    <asp:Label runat="server" ID="lblDiagnoticStep3" Text="Motivo: " style="font-weight:bold"></asp:Label>
                                                    <asp:Label runat="server" ID="lblDiagnosticStep3"> - </asp:Label>
                                                </div>
                                                <div class="col-md-12" style="margin-top:15px">
                                                    <asp:Label runat="server"  Text="Subsecuente: " style="font-weight:bold"></asp:Label>
                                                    <asp:Label runat="server" ID="lblSubsequent"> - </asp:Label>
                                                </div>
                                                <div class="col-md-12"><br />
                                                    <asp:Label  runat="server" Text="Archivo: " style="font-weight:bold"></asp:Label>
                                                    <asp:Label  runat="server" ID="lblarchivo" style="color:black;font-size:14px; cursor:pointer" class="step2Field" Text=""> </asp:Label>
                                                   
                                                    <div class="col-md-12">
                                                    <br />

                                                        <div class="col-md-1 text-right">
                                                            <i class="fa fa-file-pdf-o text-red text-right" style="font-size:25px;"></i>
                                                        </div>
                                                        <div class="col-md-6 col-md-offset-0">
                                                            <asp:FileUpload ID="FileDisabilities" runat="server" style="cursor:pointer" accept=".pdf" Class="step2Field" OnLoad="FileDisabilities_Load" />
                                                        </div>
                                                    </div>                                                                                                          
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                
                                </div>   
                            </div>
                            <div class="panel-footer" style=" height:50px;">                                
                                <asp:LinkButton ID="btnCloseStp3" CssClass="btn btn-secondary nextBtn pull-left" runat="server" OnClick="btnCloseStp1_Click">Cerrar</asp:LinkButton>                                    
                                <asp:LinkButton runat="server" ID="btnSaveDisability" class="btn btn-primary nextBtn pull-right" type="button" OnClick="btnSaveDisability_Click" style="visibility:hidden">Guardar</asp:LinkButton>                                                                                                  
                            </div>
                        </div>          
                        
                        <%-- |||||||||||||||||| Start STEP 4 ||||||||||||||||||||||||||||||| --%>
                        <div class="panel panel-default setup-content" id="step-4" style="height:590px">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="font-weight:bold">Vista previa</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">btnSaveDisability
                                            <div class="col-md-offset-2 col-md-8" style="margin-top:20px;background-color:#E5ECF8; padding-top:30px;padding-bottom:30px;">
                                                <div class="row">
                                                     <div class="col-md-12">
                                                        <div class="text-center">
                                                            <img src="assets/img/avatar2.png" class="img img-rounded" style="width:100px;margin-top:25px;" id="Img7" runat="server" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row" style="margin-top:40px">
                                                            <div class="col-md-5">
                                                                <p><asp:Label runat="server" ID="lblClockStep4" style="font-weight:bold"> Número de reloj</asp:Label></p>
                                                       
                                                                <p><asp:Label runat="server" ID="lblNameStep4" style="font-weight:bold">Nombre</asp:Label></p>

                                                                <p><asp:Label runat="server" ID="lblAreaStep4" style="font-weight:bold">Area</asp:Label></p>

                                                                <p><asp:Label runat="server" ID="lblShiftStep4" style="font-weight:bold">Turno</asp:Label></p>

                                                            </div>
                                                            <div class="col-md-7">
                                                                <p><asp:Label runat="server" ID="clockStep4">40422656</asp:Label></p>

                                                                <p><asp:Label runat="server" ID="nameStep4">Lucero marin gonzalez</asp:Label></p>

                                                                <p><asp:Label runat="server" ID="areaStep4">Sistemas</asp:Label></p>

                                                                <p><asp:Label runat="server" ID="shiftStep4">1</asp:Label></p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-2 col-md-8" style="margin-top:20px; padding-top:30px;padding-bottom:30px">
                                                <div class="col-md-12">
                                                    <asp:Label runat="server" ID="Label1" Text="Número de incapacidad:" style="font-weight:bold"></asp:Label>
                                                    <asp:Label runat="server" ID="Label2"></asp:Label>
                                                </div>
                                                <div class="col-md-12" style="margin-top:15px">
                                                    <asp:Label runat="server" ID="Label3" Text="Fecha de Inicio:" style="font-weight:bold"></asp:Label>
                                                    <asp:Label runat="server" ID="Label4"></asp:Label>
                                                </div>
                                                <div class="col-md-12" style="margin-top:15px">
                                                    <asp:Label runat="server" ID="Label5" Text="Fecha de Terminación:" style="font-weight:bold"></asp:Label>
                                                    <asp:Label runat="server" ID="Label6"></asp:Label>
                                                </div>
                                                <div class="col-md-12" style="margin-top:15px">
                                                    <asp:Label runat="server" ID="Label7" Text="Motivo:" style="font-weight:bold"></asp:Label>
                                                    <asp:Label runat="server" ID="Label8"></asp:Label>
                                                </div>
                                                <div class="col-md-12" style="margin-top:15px">
                                                    <asp:Label runat="server" ID="Label9" Text="Archivo:" style="font-weight:bold"></asp:Label>
                                                    <asp:Label runat="server" ID="NameArchivoStep4"></asp:Label>
                                                </div>
                                                <div class="col-md-4 col-md-offset-4"><br /><br /><br />
                                                    <button class="btn btn-primary nextBtn pull-right" type="button">Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>      
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="assets/dist/js/medical_disability.js"></script>
</asp:Content>

