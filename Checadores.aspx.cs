﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
public partial class _Default : System.Web.UI.Page
{
    data sqlquery = new data();
    DataTable tabla;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            if (Session["is_online"].ToString().Trim() != "")
            {
                if (!IsPostBack)
                {
                    //TextBox2.Text=DateTime.Now.ToShortDateString();
                    //TextBox3.Text = DateTime.Now.ToShortDateString();
                    txtStart_Date.Text = DateTime.Now.ToString("yyyy-MM-dd");
                    txtDue_Date.Text = DateTime.Now.ToString("yyyy-MM-dd");
                    string myvar = DateTime.Now.ToString();
                    myvar = myvar.Substring(0, 9);
                    tabla = new DataTable();
                    tabla = sqlquery.getDatatablebyQueryLTTS("SELECT CONVERT(VARCHAR, A.FECHA_CHQ, 6) 'FECHA_CHQ', A.HORA_CHQ, (SELECT B.numemp FROM Empleado B WHERE A.emp_id = B.emp_id) 'NUM_RELOJ',  A.CPL_ID from carga A WHERE A.fecha_chq BETWEEN '" + txtStart_Date.Text.Trim() + "' AND '" + txtDue_Date.Text.Trim() + "' ORDER BY fecha_chq  ,  hora_chq ");
                    FillCheckTable(tabla);
                    //GridView1.DataSource = tabla;
                    //GridView1.DataBind();
                    // GridView1.Attributes.Add("style", "table-layout:fixed");
                    //  GridView1.AutoGenerateColumns = true;

                }
                else
                {
                    tabla = Session["tabla"] as DataTable;
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception)
        {
            Response.Redirect("Login.aspx");
        }
        
        //Response.Write("<script>jQuery('#checkTableID').dataTable();</script>");

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string query = "SELECT TOP 1 emp_id FROM empleado WHERE numemp='" + txtClock_Number.Text + "' ORDER BY emp_id DESC";
        string var = sqlquery.getStringValueLTTS(query);
        //string var = sqlquery.getStringValueLTTS("SELECT emp_id FROM empleado WHERE numemp='"+txtClock_Number.Text.Trim()+"'").ToString(); // Conversion de ClockNumber a EMployeeID
        //string var = sqlquery.getDatatablebyQueryLTTS("SELECT emp_id FROM empleado WHERE numemp='"+TextBox1.Text.Trim()+"'").Rows[0][0].ToString();
        if (var.Length > 0)
        {
            tabla = sqlquery.getDatatablebyQueryLTTS("SELECT CONVERT(VARCHAR,A.FECHA_CHQ,6) 'FECHA_CHQ', A.HORA_CHQ, (SELECT B.numemp FROM Empleado B WHERE A.emp_id = B.emp_id ) 'NUM_RELOJ',  A.CPL_ID  from carga A WHERE   A.emp_id='" + var + "' AND A.fecha_chq  BETWEEN '" + txtStart_Date.Text.Trim() + "' AND '" + txtDue_Date.Text.Trim() + "' ORDER BY fecha_chq  ,  hora_chq ");
        }
        else
        {
            tabla = sqlquery.getDatatablebyQueryLTTS("SELECT CONVERT(VARCHAR,A.FECHA_CHQ,6) 'FECHA_CHQ', A.HORA_CHQ, (SELECT B.numemp FROM Empleado B WHERE A.emp_id = B.emp_id ) 'NUM_RELOJ',  A.CPL_ID  from carga A WHERE    A.fecha_chq  BETWEEN '" + txtStart_Date.Text.Trim() + "' AND '" + txtDue_Date.Text.Trim() + "' ORDER BY fecha_chq  ,  hora_chq ");
        }
        
        if (tabla.Rows.Count > 0)
        {
            FillCheckTable(tabla);
            //GridView1.DataSource = tabla;
            //GridView1.DataBind();
            // GridView1.Attributes.Add("style", "table-layout:fixed");
            // GridView1.AutoGenerateColumns = true;

            //lblLogs.InnerHtml = "SELECT A.FECHA_CHQ, A.HORA_CHQ, (SELECT B.numemp FROM Empleado B where A.emp_id = B.emp_id ) 'NUM_RELOJ',  A.CPL_ID  FROM carga A WHERE   A.emp_id='" + var + "' and A.fecha_chq  BETWEEN '" + txtStart_Date.Text.Trim() + "' AND '" + txtDue_Date.Text.Trim() + "' ORDER BY fecha_chq  ,  hora_chq ";
        }
        else
        {
            tbodyCheckTable.InnerHtml = "<tr><td colspan=\"4\" class=\"text-center\"><i class=\"fa fa-warning text-orange\"></i>&nbsp;No se encontraron checadas de los datos seleccionados</td><tr>";
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        ExcelPackage pck = new ExcelPackage();
        //string var = sqlquery.regresanumreloj(TextBox1.Text.Trim());
        //if (var.Length > 0)
        //{
        //    tabla = sqlquery.regresa_query("select A.FECHA_CHQ, A.HORA_CHQ, (select B.numemp from Empleado B where A.emp_id = B.emp_id ) 'NUM_RELOJ'  from carga A where   A.emp_id='" + var + "' and A.fecha_chq  between '" + TextBox2.Text.Trim() + "' and '" + TextBox3.Text.Trim() + "' order by fecha_chq  ,  hora_chq ");
        //}
        //else
        //{
        //    tabla = sqlquery.regresa_query("select A.FECHA_CHQ, A.HORA_CHQ, (select B.numemp from Empleado B where A.emp_id = B.emp_id ) 'NUM_RELOJ'  from carga A where    A.fecha_chq  between '" + TextBox2.Text.Trim() + "' and '" + TextBox3.Text.Trim() + "' order by fecha_chq  ,  hora_chq ");
        //}
        //GridView1.DataSource = tabla;
        //GridView1.DataBind();

        //tabla = GridView1.DataSource as DataTable;
        var ws = pck.Workbook.Worksheets.Add("Asistencia");

        ws.Cells[1, 1].Value = "Fecha";//"Num-Parte";
        ws.Cells[1, 2].Value = "Hora";
        ws.Cells[1, 3].Value = "Num Reloj";
        ws.Cells[1, 4].Value = "checador";


        ws.Cells[1, 1].Style.Font.Bold = true;
        ws.Cells[1, 2].Style.Font.Bold = true;
        ws.Cells[1, 3].Style.Font.Bold = true;
        ws.Cells[1, 4].Style.Font.Bold = true;
        ws.Cells[1, 5].Style.Font.Bold = true;


        int x = 2, y = 1;
        for (int row = 0; row <= tabla.Rows.Count - 1; row++)
        {
            for (int cell = 0; cell <= tabla.Columns.Count - 1; cell++)
            {
                ws.Cells[x, y].Value = tabla.Rows[row][cell].ToString();
                y++;
            }
            y = 1;
            x++;
        }
        ws.Column(1).AutoFit();
        ws.Column(2).AutoFit();
        ws.Column(3).AutoFit();
        ws.Column(4).AutoFit();
        ws.Column(5).AutoFit();

        pck.SaveAs(Response.OutputStream);
        //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-disposition", "attachment;  filename=ReporteAsistencia.xlsx");
        Response.End();

    }
    protected void FillCheckTable(DataTable table)
    {
        Session["tabla"] = table;
        //GridView1.DataSource = table;
        //GridView1.DataBind();
        string allRows = "";
        foreach (DataRow row in table.Rows)
        {
            allRows += addRowCheckTable(row);
        }

        if (!allRows.Equals(""))
        {
            tbodyCheckTable.InnerHtml = allRows;
        }
    }

    protected string addRowCheckTable(DataRow row)
    {
        string innerRow = "";
        innerRow += "<tr>";
        innerRow += "<td class='text-center'>" + row["FECHA_CHQ"].ToString().Trim() + "</td>";
        innerRow += "<td class='text-center'>" + row["HORA_CHQ"].ToString().Trim() + "</td>";
        innerRow += "<td class='text-center'>" + row["NUM_RELOJ"].ToString().Trim() + "</td>";
        innerRow += "<td class='text-center'>" + row["CPL_ID"].ToString().Trim() + "</td></tr>";
        return innerRow;
    }
}