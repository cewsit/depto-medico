﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;

public partial class _Medical_disability : System.Web.UI.Page
{
    data SqlQuery = new data();
    string query;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["is_online"] != null)
        {
            getDateMetricsFilter();            

            if (!IsPostBack)
            {                
                txtMetricsDate.Text = DateTime.Now.ToString("yyyy-MM");
                txtSearchClockNumberBD.Focus();
                getDisabilityReasons();
                getFilterbar();
            }

            getDisability();

            validateDatePicker();

            if (Request.Params["mode"] != null)
            {
                if (Request.Params["mode"].ToString().Trim() == "assign")
                {
                    lblMdlClockNumber.Text = Request.Params["clocknumber"].ToString();

                    mdlhistory.Attributes.Add("style", "display:block");
                    getHistory();
                }
                if (Request.Params["mode"].ToString().Trim() == "newDisability")
                {
                    mdlNewDisabilities.Attributes.Add("style", "display:block");

                    txtSearchClockNumberBD.Focus();

                    if (txtClockNumberEmployeeBD.Text!="")
                    {
                        Captura.Style.Add("visibility", "");
                    }

                    if (txtNameDisability.Text.Trim()!="")
                    {
                        Archivo.Style.Add("visibility","");
                    }
                }
                if (Request.Params["mode"].ToString().Trim() == "deleteregister")
                {
                    mdlDeleteDisability.Attributes.Add("style", "display:block;");

                    DataTable tblDeleteInfo = SqlQuery.getDatatablebyQueryHC("SELECT D.disability_name 'disability_name' ,D.clock_number 'clock_number',D.employee_name 'employee_name',D.register_date 'register_date',D.last_update 'last_update',D.employee_shift 'employee_shift',D.employee_area 'employee_area' ,D.disability_startdate 'disability_startdate', D.disability_duedate 'disability_duedate',DR.reason 'reason',D.subsequent 'subsequent' FROM disabilities D LEFT JOIN medical_disabilities_reasons DR ON D.disability_reason = DR.id WHERE disability_name = '" + Request.Params["disability_name"] + "'");

                    lblDisabilityInformation.Attributes.Add("style", "color:black");
                    lblDisabilityInformation.InnerHtml = "<h3><span class=\"label label-primary\">" + Request.Params["disability_name"] + "</span><br/></h3>";

                    foreach (DataRow row in tblDeleteInfo.Rows)
                    {
                        lblDisabilityInformation.InnerHtml += "<br/>"+row["clock_number"].ToString().Trim() + "<br/>";
                        lblDisabilityInformation.InnerHtml += row["employee_name"].ToString().Trim() + "<br/>";
                        lblDisabilityInformation.InnerHtml += "<br/>" + row["reason"].ToString().Trim() + "<br/><br/>";
                        lblDisabilityInformation.InnerHtml += "Inicio: " + row["disability_startdate"].ToString().Trim()+"<br/>";
                        lblDisabilityInformation.InnerHtml += "     Fin: " + row["disability_duedate"].ToString().Trim() + "<br/><br/>";
                        lblDisabilityInformation.InnerHtml += "Subsecuente " + row["subsequent"].ToString().Trim();
                    }
                }
            }

            
            
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        ComposeTableDisability();
        ComposeTableHistory();
    }

    protected void getDisability()
    {
        dplDisabilityStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#24719E"); //#24719E
        dplDisabilityStatus.ForeColor = System.Drawing.Color.White;

        DataTable table = SqlQuery.getDatatablebyQueryHC("SELECT D.disability_name 'Incapacidad','' 'photo', D.clock_number 'Reloj', D.employee_name 'Nombre', D.employee_shift 'Turno', D.employee_area 'Departamento', CONVERT (DATE,D.disability_startdate,104) 'Inicia', D.disability_duedate 'Termina', DR.reason 'Tipo', D.subsequent 'Subsecuente',HC.asistencia 'Asistencia', '' 'Accion' FROM disabilities D LEFT JOIN medical_disabilities_reasons DR ON D.disability_reason = DR.id LEFT JOIN [HEADCOUNT].[dbo].[HEADCOUNT] HC ON HC.Numreloj = D.clock_number WHERE CONVERT(DATE,GETDATE()) < CONVERT(DATE,D.disability_duedate) ORDER BY disability_startdate DESC");

        gdvDisability.DataSource = table;
        gdvDisability.DataBind();

        if (gdvDisability.Rows.Count > 0)
        {
            EmplyTableLog.InnerHtml = "";
        }
        else
        {
            EmplyTableLog.InnerHtml = "<b><h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron registros</h5></b>";
        }
    }

    protected string addRowBodyHeadCountTable(DataRow row,string trClass,string tdClass)
    {
        string rowString = "";
        string ImagePath = URLExists(Session["url_photos"].ToString() + row["Numreloj"].ToString().Trim() + ".jpg");
        string fotoUrl = "<img src='" + ImagePath + "' width='60px' class='img-thumbnail zoom'/>";
        rowString = "<tr class='"+trClass+"'"+
            "<td class='" + tdClass + "'>" + fotoUrl + "</td>" +
            "</tr>";

        return rowString;
    }

    static public string URLExists(string url)
    {
        string result = url;
        return result;
    }

    protected void gdvDisability_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string innerStringRegisters = "";

        int nameDisability = 0;
        int photo = 1;
        int clock = 2;
        int name = 3;
        int shift = 4;
        int area = 5;
        int start = 6;
        int end = 7;
        int reason = 8;
        int subsecuent = 9;
        int attendance = 10;
        int action = 11;

        string rowString = "";
        string ImagePath = URLExists(Session["url_photos"].ToString() + e.Row.Cells[clock].Text + ".jpg");
        string fotoUrl = "<img src='" + ImagePath + "' width='60px' class='img-thumbnail zoom'/>";

        if (e.Row.RowType != DataControlRowType.Header)
        {   
            e.Row.Cells[photo].Text = fotoUrl;

            string attendance_string = e.Row.Cells[attendance].Text;


            if (attendance_string == "1")
            {
                e.Row.Cells[attendance].Text = "<i class=\"fa fa-check-circle text-green\"></i>";
            }
            else
            {
                e.Row.Cells[attendance].Text = "<i class=\"fa fa-times-circle text-red\"></i>";
            }

            e.Row.Cells[action].Text = "<a href=\"assets/disability_files/" + e.Row.Cells[nameDisability].Text.Trim() + ".pdf\" target=\"_BLANK\"><i title='Document' class=\"text-red fa-file-pdf-o fa\" style=\"color:white; font-size:18px;\"></i><a>&nbsp;";
            //e.Row.Cells[action].Text = "<a href=\"Medical_disability.aspx?mode=deleteregister&disability_name=" + e.Row.Cells[nameDisability].Text + "\"><i title='Eliminar' class=\"text-red fa fa-trash fa-fw\" style=\"color:white; font-size:18px;\"></i><a>&nbsp;";
        }
    }

    protected void getHistory()
    {
        DataTable table = SqlQuery.getDatatablebyQueryHC("SELECT ICP.name_disability 'Incapacidad', CONVERT (VARCHAR(10),start_date,110) 'Inicio',CONVERT (VARCHAR(10),due_date,110) 'Fin',ICP.reason 'Motivo','' 'Archivo' FROM Disabilities ICP WHERE clocknumber_employee = '" + lblMdlClockNumber.Text + "' ORDER BY due_date ASC");
        gdvHistory.DataSource = table;
        gdvHistory.DataBind();
    }

    protected void ComposeTableDisability()
    {
        if (gdvDisability.Rows.Count > 0)
        {
            gdvDisability.UseAccessibleHeader = true;
            gdvDisability.HeaderRow.TableSection = TableRowSection.TableHeader;
            gdvDisability.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    protected void ComposeTableHistory()
    {
        if (gdvHistory.Rows.Count > 0)
        {
            gdvHistory.UseAccessibleHeader = true;
            gdvHistory.HeaderRow.TableSection = TableRowSection.TableHeader;
            gdvHistory.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    protected string getDays(string dueDate)
    {
        int Days = 0;

        string StringDays = "";
        if (dueDate != "&nbsp;")
        {
            DateTime dueDateTime = DateTime.Parse(dueDate);
            DateTime Now = DateTime.Today;
            TimeSpan ts = dueDateTime - Now;
            Days =  ts.Days;
        }
        else
        {
            Days = 0;
        }
        

        StringDays =  (Days < 0  ? 0 : Days) + " dias" ;

        return StringDays ;

    }

    protected string getStatusDisability(string dueDate)
    {
        string Active = "<span class=\"label bg-green\">Active</span>";
        string Disabled = "<span class=\" label bg-red\">Inactive</span>";

        if (dueDate != "&nbsp;")
        {
            DateTime dueDateTime = DateTime.Parse(dueDate);
            if (dueDateTime >= DateTime.Today) 
            {
                return Active;
            }

            else
            {
                return Disabled;
            }
        }
        else
        {
            return "";
        }
    }

    protected void Getmetrics(string year,string month)
    {
        //agregar filtrado a los metricos
        lblEnfGeneral.InnerHtml = "<h3>"+SqlQuery.getIntValueHC("SELECT COUNT(*) FROM disabilities WHERE disability_reason = '1' AND YEAR (disability_startdate) = YEAR ('" + year + "') AND MONTH(disability_startdate) = MONTH ('"+year+"-"+month+"-01')").ToString()+"</h3>";
        lblMaternidad.InnerHtml = "<h3>" + SqlQuery.getIntValueHC("SELECT COUNT (*) FROM disabilities WHERE disability_reason = '2' AND YEAR (disability_startdate) = YEAR ('" + year + "') AND MONTH(disability_startdate) = MONTH ('" + year+"-"+month+"-01')").ToString() + "</h3>";
        lblRgTrayecto.InnerHtml = "<h3>" + SqlQuery.getIntValueHC("SELECT COUNT (*) FROM disabilities WHERE disability_reason = '3' AND YEAR (disability_startdate) = YEAR ('" + year + "') AND MONTH(disability_startdate) = MONTH ('" + year+"-"+month+"-01')").ToString() + "</h3>";
        lblRgTrabajo.InnerHtml = "<h3>" + SqlQuery.getIntValueHC("SELECT COUNT (*) FROM disabilities WHERE disability_reason = '4' AND YEAR (disability_startdate) = YEAR ('" + year + "') AND MONTH(disability_startdate) = MONTH ('" + year+"-"+month+"-01')").ToString() + "</h3>";
        lblCovid.InnerHtml = "<h3>" + SqlQuery.getIntValueHC("SELECT COUNT (*) FROM disabilities WHERE disability_reason = '6' AND YEAR (disability_startdate) = YEAR ('" + year + "') AND MONTH(disability_startdate) = MONTH ('" + year+"-"+month+"-01')").ToString() + "</h3>";
    }

    protected string SentenceCase(string Sentence)
    {
        TextInfo textinfo = new CultureInfo("en-US", false).TextInfo;
        Sentence = textinfo.ToTitleCase(Sentence.ToLower());
        return Sentence;
    }

    protected void btnSearchClockNumberBD_Click(object sender, EventArgs e)
    {
        string query = "SELECT Numreloj,Nombre,Turno,Depto FROM HEADCOUNT WHERE Numreloj = '"+txtSearchClockNumberBD.Text+"'";
        
        DataTable tableEmployee = SqlQuery.getDatatablebyQueryHC(query);

        if(tableEmployee.Rows.Count > 0)
        {
            foreach (DataRow row in tableEmployee.Rows)
            {
                imageEmployeeBD.Attributes.Add("src", Session["url_photos"].ToString() + row["Numreloj"].ToString().Trim() + ".jpg");
                txtClockNumberEmployeeBD.Text = row["Numreloj"].ToString();
                txtNameEmployeeBD.Text = SentenceCase(row["Nombre"].ToString());
                txtTurnEmployeeBD.Text = row["Turno"].ToString();
                txtAreaNameBD.Text = row["Depto"].ToString();
                btnNextFirstStep.Visible = true;
                txtSearchClockNumberBD.Text = "";
                txtSearchClockNumberBD.Focus();

                imageEmployeeBDstp2.Attributes.Add("src", Session["url_photos"].ToString() + row["Numreloj"].ToString().Trim() + ".jpg");
                lblClockNumberStp2.Text = row["Numreloj"].ToString();
                lblEmployeeName.Text = SentenceCase(row["Nombre"].ToString()); ;
                lblEmployeeArea.Text = row["Depto"].ToString();
                lblShift.Text = row["Turno"].ToString();

                imageEmployeeBDstp4.Attributes.Add("src", Session["url_photos"].ToString() + row["Numreloj"].ToString().Trim() + ".jpg");
                lblClockNumberStp4.Text = row["Numreloj"].ToString();
                lblEmployeeNameStp4.Text = SentenceCase(row["Nombre"].ToString()); ;
                lblDeptoCodeStp4.Text = row["Depto"].ToString();
                lblShiftStp4.Text = row["Turno"].ToString();

                getSubsequentEmployee();

            }
            stp1Logs.InnerHtml = "<span class=\"label bg-green\" style=\"font-size:13px;text-align:center\"> Usuario encontrado </span>";
            btnNextFirstStep.Focus();

        }
        else
        {
            txtSearchClockNumberBD.Text = "";
            stp1Logs.InnerHtml = "<span class=\"label bg-red\" style=\"font-size:13px;text-align:center\"> El usuario no existe </span>";
            txtClockNumberEmployeeBD.Text = "";
            txtNameEmployeeBD.Text = "";
            txtAreaNameBD.Text = "";
            txtTurnEmployeeBD.Text = "";
            btnNextFirstStep.Visible = false;
            imageEmployeeBD.Attributes.Add("src", "assets/img/avatar2.png");
        }
    }

    protected void getSubsequentEmployee()
    {
        try
        {
            dplSubsequent.Items.Clear();
            dplSubsequent.AppendDataBoundItems = true;
            DataTable subsecuent = SqlQuery.getDatatablebyQueryHC("SELECT disability_name, CONCAT(disability_name,' ( ',CONVERT(DATE,disability_startdate),' - ',CONVERT(DATE,disability_duedate),')') 'concate' FROM disabilities WHERE clock_number = '" + txtClockNumberEmployeeBD.Text.Trim() + "' ORDER BY disability_duedate DESC");

            dplSubsequent.DataSource = subsecuent;
            dplSubsequent.DataTextField = "concate";
            dplSubsequent.DataValueField = "disability_name";
            dplSubsequent.DataBind();
            
            dplSubsequent.Items.Insert(0,"Sin subsecuente");
            
        }
        catch (Exception)
        { }
    }

    protected void gdvHistory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int File = 7;

        string Fileuploads = ""; /// seleccionar el archivo con el nombre de la carpeta

        if (e.Row.RowType != DataControlRowType.Header)
        {
            gdvHistory.HeaderStyle.BackColor = System.Drawing.Color.LightGray;
            e.Row.Cells[0].BackColor = System.Drawing.Color.Lavender;
        }

        foreach (DataRow row in SqlQuery.getDatatablebyQueryHC("SELECT Numreloj,Nombre,Linea FROM HEADCOUNT WHERE Numreloj='" + lblMdlClockNumber.Text + "'").Rows)
        {
            ImgHistory.Attributes.Add("src", Session["url_photos"].ToString() + row["Numreloj"].ToString().Trim() + ".jpg");
            lblMdlName.Text = row["Nombre"].ToString();
            lblMdlArea.Text = row["Linea"].ToString();
        }
    }

    protected void btnCloseHistory_Click(object sender, EventArgs e)
    {
        Response.Redirect("Medical_disability.aspx");
    }

    protected void ClearControls()
    {
        txtNameDisability.Text = "";
        txtStarDate.Text = "";
        txtFinishDate.Text = "";
        txtClockNumberEmployeeBD.Text = "";
        txtAreaNameBD.Text = "";
        txtNameEmployeeBD.Text = "";
        txtTurnEmployeeBD.Text = "";
    }

    protected void getDisabilityReasons()
    {
        DataTable disabilitiesReasons = SqlQuery.getDatatablebyQueryHC("SELECT id, reason FROM medical_disabilities_reasons WHERE active='1' ORDER BY id ASC");
        try
        {
            dplDisabilitiesReasons.Items.Clear();
            dplDisabilitiesReasons.AppendDataBoundItems = true;            
            dplDisabilitiesReasons.DataSource = disabilitiesReasons;
            dplDisabilitiesReasons.DataTextField = "reason";
            dplDisabilitiesReasons.DataValueField = "id";
            dplDisabilitiesReasons.DataBind();           
        }
        catch (Exception)
        { }        
    }
    
    protected void CbxAppointment_CheckedChanged(object sender, EventArgs e)
    {
        if (CbxAppointment.Checked == true)
        {
            dplSubsequent.Visible = true;
            dplSubsequent.SelectedIndex = 1;
        }
        else
        {
            dplSubsequent.Visible = true;
            dplSubsequent.SelectedIndex = 0;
        }
    }

    protected void SelectionDisabilitiesSubsecuent (string Subsecuent)
    {
        dplSubsequent.Items.Insert(0, new ListItem("", String.Empty));
        
        ListItem list = dplSubsequent.Items.FindByValue(Subsecuent);

        if (list != null)
        {
            list.Selected = true;
        }
        else
        {

            alert.InnerHtml = "<span class=\"label bg-red\" style=\"font-size:13px;text-align:center\"> Seleccion invalida </span>";
        }
    }

    protected void dplNumberDisability_SelectedIndexChanged(object sender, EventArgs e)
    {
        string NameDisabilities = dplSubsequent.SelectedValue;
        if(NameDisabilities != "" || NameDisabilities != null)
        {
            dplSubsequent.Text = NameDisabilities;
            dplSubsequent.Focus();
        }
    }

    protected void btnSaveDisability_Click(object sender, EventArgs e)
    {
        if (txtNameDisability.Text == "" || txtStarDate.Text == "" || txtFinishDate.Text == "" ||  FileDisabilities.HasFile==false)
        {
            alert.InnerHtml = "<span class=\"label bg-red\" style=\"font-size:13px;text-align:center\"> Todos los campos son requeridos</span>";
            if (FileDisabilities.HasFile)
            {

            }
            else
            {
                alert.InnerHtml = "<span class=\"label bg-orange\" style=\"font-size:13px;text-align:center\">Faltan campos por llenar.</span";
            }
        }
        else
        {
            //CARGAR EL ARCHIVO DESDE EL BOTON GUARDAR

            if (FileDisabilities.HasFile)
            {
                string imagePath = string.Format("~/assets/disability_files/{0}"+Path.GetExtension(FileDisabilities.FileName), txtNameDisability.Text.Trim());
                FileDisabilities.SaveAs(Server.MapPath(imagePath));

                string subsequent = "";
                if (dplSubsequent.SelectedIndex == 0)
                {
                    subsequent = "Inicial";
                }
                else
                {
                    subsequent = dplSubsequent.SelectedValue.ToString();
                }

                string query = "INSERT INTO disabilities OUTPUT 1 VALUES('" + txtNameDisability.Text + "','" + lblClockNumberStp4.Text.Trim() + "','" + lblEmployeeNameStp4.Text.Trim() + "',SYSDATETIME(),SYSDATETIME(),'" + lblShiftStp4.Text.Trim() + "','" + lblDeptoCodeStp4.Text.Trim() + "','" + txtStarDate.Text + "','" + txtFinishDate.Text + "','" + dplDisabilitiesReasons.SelectedValue + "','" + Session["userid"].ToString() + "','" + subsequent + "')";

                if (SqlQuery.getIntValueHC(query) == 1)
                {
                    txtNameDisability.Focus();
                    validateDatePicker();
                    mdlNewDisabilities.Attributes.Add("style", "display:none");
                }
                ClearControls();
            }
            else
            {
                alert.InnerHtml = "<span class=\"label bg-orange\" style=\"font-size:13px;text-align:center\">Selecciona un archivo</span>";
            }
        }
    }

    protected void txtNameDisability_TextChanged(object sender, EventArgs e)
    {
    }

    protected void fnSetNewControls_Click(object sender, EventArgs e)
    {
        Response.Write("<script>alert('test')</script>");
    }

    protected void FileDisabilities_Load(object sender, EventArgs e)
    {
        lblarchivo.Text = FileDisabilities.FileName;

        if (FileDisabilities.HasFile)
        {
            btnSaveDisability.Visible = true;
        }
    }

    protected void btnCloseStp1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Medical_disability.aspx");
    }

    protected void btnDeleteDisability_Click(object sender, EventArgs e)
    {
        string Query = "DELETE FROM disabilities OUTPUT 1 WHERE disability_name = '" + Request.Params["disability_name"] + "'";
        if (SqlQuery.getIntValueHC(Query) == 1)
        {
            Response.Redirect("Medical_disability.aspx");
        }
        else
        {
            lblDisabilityInformation.InnerHtml += "<br/><br/>ERROR:<br/> " + Query + "";
        }
    }

    protected void btnCloseMdlValidate_Click(object sender, EventArgs e)
    {
        Response.Redirect("Medical_disability.aspx");
    }

    protected static Boolean isDate(string date)
    {
        try
        {
            DateTime.Parse(date);
            return true;
        }
        catch
        {
            return false;
        }
    }

    protected void validateDatePicker()
    {
        DateTime startDate = DateTime.MinValue;
        DateTime dueDate = DateTime.Today;

        if (isDate(txtStartDate.Text.Trim()))
        {
            if (isDate(txtDueDate.Text.Trim()))
            {
                startDate = DateTime.Parse(txtStartDate.Text.Trim());
                dueDate = DateTime.Parse(txtDueDate.Text.Trim());
            }
        }

        fillTableSM(startDate, dueDate);
        //drawingColor();
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        //fillTableSM(getQueryResults());
        validateDatePicker();
    }

    protected void txtDisabilityName_TextChanged(object sender, EventArgs e)
    {
        validateDatePicker();
        drawingColor();
    }

    protected void txtClockNumber_TextChanged(object sender, EventArgs e)
    {
        validateDatePicker();
        drawingColor();
    }

    protected void dplDisabilityType_SelectedIndexChanged(object sender, EventArgs e)
    {
        validateDatePicker();
        drawingColor();
    }

    protected void dplDisabilityStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        validateDatePicker();
        drawingColor();
    }

    protected void fillTableSM(DateTime startDate, DateTime dueDate)
    {
        DataTable table = SqlQuery.getDatatablebyQueryHC(getQueryResults(startDate, dueDate));
        gdvDisability.DataSource = table;
        if (table.Rows.Count <= 0)
        {
            EmplyTableLog.InnerHtml = "<b><h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron registros con estos criterios: "+txtDisabilityName.Text.Trim()+" "+txtClockNumber.Text.Trim()+" "+dplDisabilityType.SelectedItem.Text+" "+dplDisabilityStatus.SelectedValue+"</h5></b>";
        }
        else
        {
            EmplyTableLog.InnerHtml = "";
        }

        gdvDisability.DataBind();

        if (gdvDisability.Rows.Count > 0)
        {
            EmplyTableLog.InnerHtml = "&nbsp;";
        }
        else
        {
            EmplyTableLog.InnerHtml = "<b><h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron registros</h5></b>";
        }
    }

    protected string getQueryResults(DateTime startDate, DateTime dueDate)
    {
        query = "SELECT D.disability_name 'Incapacidad', '' 'Foto', D.clock_number 'Reloj', D.employee_name 'Nombre', D.employee_shift 'Turno', D.employee_area 'Departamento',  CONVERT(VARCHAR(10),D.disability_startdate,111) 'Inicia', CONVERT(VARCHAR(10),D.disability_duedate,111) 'Termina', DR.reason 'Tipo', D.subsequent 'Subsecuente',HC.asistencia 'Asistencia', 'Accion' 'Accion' FROM disabilities D LEFT JOIN medical_disabilities_reasons DR ON D.disability_reason = DR.id LEFT JOIN [HEADCOUNT].[dbo].[HEADCOUNT] HC ON HC.Numreloj = D.clock_number  WHERE ";

        if (txtDisabilityName.Text.Trim() != "")
        {
            query += " D.disability_name ='" + txtDisabilityName.Text.Trim() + "'";
        }
        else
        {
            query += " (1=1)";
        }

        if (txtClockNumber.Text.Trim() != "")
        {
            if (txtDisabilityName.Text.Trim() != "")
            {
                query += " D.clock_number='" + txtClockNumber.Text.Trim() + "'";
            }
            else
            {
                query += " AND D.clock_number='" + txtClockNumber.Text.Trim() + "'";
            }
        }

        if (dplDisabilityType.SelectedIndex != 0)
        {
            query += " AND DR.id='" + dplDisabilityType.SelectedValue.ToString() + "'";
        }

        if (dplDisabilityStatus.SelectedIndex == 1)
        {
            query += " AND CONVERT(DATE,GETDATE()) < CONVERT(DATE,D.disability_duedate)";
        }
        else if (dplDisabilityStatus.SelectedIndex == 2)
        {
            query += " AND CONVERT(DATE,GETDATE()) > CONVERT(DATE,D.disability_duedate)";
        }

        if (txtStartDate.Text != "" && txtDueDate.Text != "")
        {
            query += " AND disability_startdate BETWEEN CONVERT ( DATE, '" + startDate + "' ) AND CONVERT ( DATE, '" + dueDate + "' )";
        }

        drawingColor();

        query = query + " ORDER BY D.disability_startdate DESC";
        Debug.WriteLine(query);
        return query;
    }

    protected void getFilterbar()
    {
        dplDisabilityStatus.Items.Insert(0, "Todas las Incapacidades");
        dplDisabilityStatus.Items.Insert(1, "Activa");
        dplDisabilityStatus.Items.Insert(2, "Inactiva");
        dplDisabilityStatus.SelectedIndex=1;

        DataTable disabilitiesReasons = SqlQuery.getDatatablebyQueryHC("SELECT id, reason FROM medical_disabilities_reasons ORDER BY id ASC");
        try
        {
            dplDisabilityType.Items.Clear();
            dplDisabilityType.AppendDataBoundItems = true;
            dplDisabilityType.DataSource = disabilitiesReasons;
            dplDisabilityType.DataTextField = "reason";
            dplDisabilityType.DataValueField = "id";
            dplDisabilityType.DataBind();
            dplDisabilityType.Items.Insert(0, "Todos los Tipos");
        }
        catch (Exception)
        { }
    }

    protected void drawingColor()
    {
        if (txtDisabilityName.Text == "")
        {
            txtDisabilityName.BackColor = System.Drawing.Color.White;
            txtDisabilityName.ForeColor = System.Drawing.Color.Black;
        }
        else
        {
            txtDisabilityName.BackColor = System.Drawing.ColorTranslator.FromHtml("#24719E"); //#24719E
            txtDisabilityName.ForeColor = System.Drawing.Color.White;
        }

        if (txtClockNumber.Text =="")
        {
            txtClockNumber.BackColor = System.Drawing.Color.White;
            txtClockNumber.ForeColor = System.Drawing.Color.Black;
        }
        else
        {
            txtClockNumber.BackColor = System.Drawing.ColorTranslator.FromHtml("#24719E"); //#24719E
            txtClockNumber.ForeColor = System.Drawing.Color.White;
        }

        if (dplDisabilityType.SelectedIndex == 0)
        {
            dplDisabilityType.BackColor = System.Drawing.Color.White;
            dplDisabilityType.ForeColor = System.Drawing.Color.Black;
        }
        else
        {
            dplDisabilityType.BackColor = System.Drawing.ColorTranslator.FromHtml("#24719E"); //#24719E
            dplDisabilityType.ForeColor = System.Drawing.Color.White;
        }

        if (dplDisabilityStatus.SelectedIndex == 0)
        {
            dplDisabilityStatus.BackColor = System.Drawing.Color.White;
            dplDisabilityStatus.ForeColor = System.Drawing.Color.Black;
        }
        else
        {
            dplDisabilityStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#24719E"); //#24719E
            dplDisabilityStatus.ForeColor = System.Drawing.Color.White;
        }
    }

    protected void btnMetricsFilter_Click(object sender, EventArgs e)
    {
        getDateMetricsFilter();        
    }

    protected void getDateMetricsFilter()
    {
        string DateyyyyMM;

        if (txtMetricsDate.Text!="")
        {
            DateyyyyMM = txtMetricsDate.Text;
        }
        else
        {
            txtMetricsDate.Text = DateTime.Now.ToString("yyyy-MM");
            DateyyyyMM = txtMetricsDate.Text;         
        }

        if (DateyyyyMM.Split('-').Length > 0)
        {
            Getmetrics(DateyyyyMM.Split('-')[0],DateyyyyMM.Split('-')[1]);
        }
    }
}