﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _EmployeeMedicalInformation : System.Web.UI.Page
{
    data SqlQuery = new data();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["is_online"] != null)
        { 
            txtHeight.Attributes.Add("Autocomplete", "off");
            txtWeight.Attributes.Add("Autocomplete", "off");
            txtTemperature.Attributes.Add("Autocomplete", "off");
            txtHeartRate.Attributes.Add("Autocomplete", "off");
            txtBodyMass.Attributes.Add("Autocomplete", "off");
            txtNumberAction.Attributes.Add("Autocomplete", "off");
            txtUserclocknumber.Attributes.Add("Autocomplete","off");
            //txtDateAppointment.Attributes.Add("Autocomplete", "off");
            
            if (!Page.IsPostBack)
            {
                AddSickness("");
                //AddReason();
                //calendarDateApointment.Visible = false;
                //txtDateAppointment.Text = DateTime.Now.ToString("yyyy-MM-dd");
                //dplRazorConsultation.Visible = false;
                //lblRazorConsultation.Visible = false;
                txtUserclocknumber.Focus();
            }
            getMedicalDisability();
            getConsultations();
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        ComposeTableConsultation();
        ComposeTableMedicalDisability();
    }
    private void ClearControls()
    {
        txtHeight.Text = "";
        txtWeight.Text = "";
        txtTemperature.Text = "";
        txtHeartRate.Text = "";
        txtBodyMass.Text = "";
        txtMedicalTreatment.Text = "";
        txtUserclocknumber.Text = "";
        txtNumberAction.Text = "";
        dplActionMedical.Text = "";
        //dplRazorConsultation.Text = "";
        //txtDateAppointment.Text = "";
        txtDiagnostic.Text = "";
    }
    //protected void AddReason()
    //{
    //    dplRazorConsultation.Items.Insert(0, new ListItem(String.Empty));
    //    ListItem i;
    //    i = new ListItem("Diabetes", "1");
    //    dplRazorConsultation.Items.Add(i);
    //    i = new ListItem("Hipertension", "2");
    //    dplRazorConsultation.Items.Add(i);
    //    i = new ListItem("Embarazo", "3");
    //    dplRazorConsultation.Items.Add(i);
    //    i = new ListItem("Otros...", "4");
    //    dplRazorConsultation.Items.Add(i);
    //}
    private void AddSickness(string defaultSelectedValue)
    {
        dplActionMedical.Items.Clear();
        DataTable table = SqlQuery.getDatatablebyQueryHC("SELECT id,sickness FROM medical_action");
        dplActionMedical.Items.Insert(0,new ListItem("", String.Empty));

        foreach(DataRow row in table.Rows)
        {
            dplActionMedical.Items.Add(new ListItem(row["sickness"].ToString(),row["id"].ToString()));
        }
        ListItem list = dplActionMedical.Items.FindByValue(defaultSelectedValue);

        if (list != null)
        {
            list.Selected = true;
            lblLogsno.InnerHtml = "";
            txtDiagnostic.Focus();
        }
        else
        {
            lblLogsno.InnerHtml = "<span class=\"text-red\">no registrado</span>";
        }
    }
    protected void imgbtnCalendar_Click(object sender, ImageClickEventArgs e)
    {
        //if (calendarDateApointment.Visible)
        //{
        //    calendarDateApointment.Visible = false;
        //}
        //else
        //{
        //    calendarDateApointment.Visible = true;
        //}
    }
    //protected void calendarDateApointment_SelectionChanged(object sender, EventArgs e)
    //{
    //    txtDateAppointment.Text = calendarDateApointment.SelectedDate.ToShortDateString();
    //    calendarDateApointment.Visible = false;
    //}
    protected void btnSaveInformation_Click(object sender, EventArgs e)
    {
        int appointment = 0;
        int attended = 0;
        string consultation_note = txtConsultationNote.Text.Trim();
        //if (CbxAppointment.Checked == true)
        //{
        //    appointment = 1;
        //}
        string Query = "INSERT INTO medical_consultation (clock_number,consultation_date,sickness,medicament,user_id,new_consultation_date,reason_consultation,glucose,weight,temperature,heartrate,blood_pressure,is_appointment,attended_appointment,diagnostic,name_employee,consultation_note) OUTPUT 1 VALUES ('" + lblclocknumber.Text.Trim() + "',SYSDATETIME(),'" + dplActionMedical.SelectedItem + "','" + txtMedicalTreatment.Text.Trim() + "','" + Session["userid"].ToString() + "','','','" + txtHeight.Text.Trim() + "','" + txtWeight.Text.Trim() + "','" + txtTemperature.Text.Trim() + "','" + txtHeartRate.Text.Trim() + "','" + txtBodyMass.Text.Trim() + "','" + appointment + "','" + attended + "','" + txtDiagnostic.Text.Trim() + "','"+ lblNameEmployee.Text.Trim() +"','"+consultation_note+"')";  /*vacios:*//*+ txtDateAppointment.Text +*/ /*+ dplRazorConsultation.SelectedItem +*/

        //if (txtDateAppointment.Text.Trim() != "" || appointment == 0)
        //    {
        if (lblclocknumber.Text == "")
                {
                    lblLogsnouser.InnerHtml = "<span class=\"label bg-red\" style=\"font-size:13px;text-align:center\"> Ingrese un número de reloj </span>";
                    btnSaveInformation.Visible = false;
                }
                else
                {
                    lblLogsSaveConsultation.InnerHtml = "";
                    
                    if (SqlQuery.getIntValueHC(Query) == 1)
                    {
                        lblLogsSaveConsultation.InnerHtml = "<span class=\"label bg-green\" style=\"font-size:13px;text-align:center\"> Consulta registrada </span>";
                        getConsultations();
                        RestartForm();
                        txtUserclocknumber.Focus();
                        //Response.Redirect("EmployeeMedicalInformation.aspx");
                    }
                }
            //}
            //else
            //{
            //    lblLogsSaveConsultation.InnerHtml = "<span class=\"text-red\"> Seleccione fecha valida </span>";
        //}
            getConsultations();
    }
    protected void RestartForm()
    {
        txtHeight.Text = "";
        txtWeight.Text = "";
        txtTemperature.Text = "";
        txtHeartRate.Text = "";
        txtBodyMass.Text = "";
        txtMedicalTreatment.Text = "";
        txtConsultationNote.Text = "";
        txtUserclocknumber.Text = "";
        txtNumberAction.Text = "";
        dplActionMedical.Text = "";
        //dplRazorConsultation.Text = "";
        //txtDateAppointment.Text = "";
        txtDiagnostic.Text = "";

        //CbxAppointment.Checked = false;
        //txtDateAppointment.Visible = false;
        //dplRazorConsultation.Visible = false;
        //lblRazorConsultation.Visible = false;

        imageEmployee.Attributes.Add("src", "assets/img/avatar2.png");
        lblclocknumber.Text = "";
        lblNameEmployee.Text = "";
    }
    protected void getConsultations()
    {
        //DataTable table = SqlQuery.getDatatablebyQueryHC("SELECT MEDCON.clock_number 'No.Reloj', MEDCON.consultation_date 'Fecha de realización',MEDCON.sickness 'Acción medica',MEDCON.diagnostic 'Diagnóstico', MEDCON.medicament 'Medicamento', MEDCON.new_consultation_date 'Proxima cita',MEDCON.reason_consultation 'Razón',MEDCON.glucose 'Glu',MEDCON.weight 'P',MEDCON.temperature 'T',MEDCON.heartrate 'FC',MEDCON.blood_pressure 'PA',MEDCON.is_appointment,MEDCON.attended_appointment FROM medical_consultation MEDCON WHERE clock_number = '" + lblclocknumber.Text + "' ORDER BY consultation_date DESC ");
        DataTable table = SqlQuery.getDatatablebyQueryHC("SELECT MEDCON.consultation_date 'Fecha',MEDCON.sickness 'Acción medica',MEDCON.diagnostic 'Diagnóstico', MEDCON.medicament 'Medicamento',MEDCON.consultation_note 'Nota',MEDCON.glucose 'G',MEDCON.weight 'P',MEDCON.temperature 'T',MEDCON.heartrate 'FC',MEDCON.blood_pressure 'PA' FROM medical_consultation MEDCON WHERE clock_number = '" + lblclocknumber.Text + "' ORDER BY consultation_date DESC ");
        GdvConsultationRegister.DataSource = table;
        GdvConsultationRegister.DataBind();
    }
    protected void ComposeTableConsultation()
    {
        if (GdvConsultationRegister.Rows.Count > 0)
        {
            GdvConsultationRegister.UseAccessibleHeader = true;
            GdvConsultationRegister.HeaderRow.TableSection = TableRowSection.TableHeader;
            GdvConsultationRegister.FooterRow.TableSection = TableRowSection.TableFooter;
        } 
    }
    protected void getMedicalDisability()
    {
        DataTable table = SqlQuery.getDatatablebyQueryHC("SELECT INC.clock_number 'No.Reloj', INC.disability_name 'No.Incapacidad', INC.register_date 'Realización', INC.employee_shift 'Turno', INC.employee_area 'Area', INC.disability_startdate 'Fecha de Inicio',INC.disability_duedate 'Fecha de Fin',R.reason 'Motivo' FROM disabilities INC LEFT JOIN medical_disabilities_reasons R ON R.id = INC.disability_reason WHERE clock_number='" + lblclocknumber.Text + "' ORDER BY disability_startdate DESC");
        GdvMedicalDisability.DataSource = table;
        GdvMedicalDisability.DataBind();
    }
    protected void ComposeTableMedicalDisability()
    {
        if(GdvMedicalDisability.Rows.Count > 0)
        {
            GdvMedicalDisability.UseAccessibleHeader = true;
            GdvMedicalDisability.HeaderRow.TableSection = TableRowSection.TableHeader;
            GdvMedicalDisability.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
    protected void btnReturnMain_Click(object sender, EventArgs e)
    {
        Response.Redirect("Main.aspx");
    }
    protected void txtNumberAction_TextChanged(object sender, EventArgs e)
    {
        AddSickness(txtNumberAction.Text.Trim());
    }
    protected string SentenceCase(string Sentence)
    {
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        Sentence = textInfo.ToTitleCase(Sentence.ToLower());
        return Sentence;
    }
    protected void btnclocknumber_Click(object sender, EventArgs e)
    {
        lblLogsSaveConsultation.InnerHtml = "";

        string query = "SELECT Numreloj,Nombre FROM HEADCOUNT WHERE Numreloj='" + txtUserclocknumber.Text + "'";
        
        if (SqlQuery.getStringValueHC(query)==null)
        {
            lblLogsnouser.InnerHtml = "<span class=\"text-red\"> Usuario dado de baja </span>";
            lblNameEmployee.Text = "";
            lblclocknumber.Text = "";
            imageEmployee.Attributes.Add("src", "assets/img/avatar2.png");
            btnSaveInformation.Visible = false;
        }
        else
        {
            lblLogsnouser.InnerText = "";

            foreach (DataRow row in SqlQuery.getDatatablebyQueryHC("SELECT Numreloj,Nombre FROM HEADCOUNT WHERE Numreloj='" + txtUserclocknumber.Text + "'").Rows)
            {
                imageEmployee.Attributes.Add("src", Session["url_photos"].ToString() + row["Numreloj"].ToString().Trim() + ".jpg");
                lblclocknumber.Text = row["Numreloj"].ToString();
                lblNameEmployee.Text = SentenceCase(row["Nombre"].ToString());
                btnSaveInformation.Visible = true;
                txtNumberAction.Focus();
            }
        }
        getConsultations();
        getMedicalDisability();
        ClearControls();
    }
    //protected void CbxAppointment_CheckedChanged(object sender, EventArgs e)
    //{
    //    if(CbxAppointment.Checked == true)
    //    {
    //        txtDateAppointment.Visible = true;
    //        dplRazorConsultation.Visible = true;
    //        lblRazorConsultation.Visible = true;
    //    }
    //    else
    //    {
    //        txtDateAppointment.Visible = false;
    //        dplRazorConsultation.Visible = false;
    //        lblRazorConsultation.Visible = false;
    //    }
    //}
    protected void GdvConsultationRegister_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //int dateIndex = 5;
        //int appointmentIndex = 12;
        //int attended = 13;

        //if (e.Row.RowType != DataControlRowType.Header)
        //{
        //    if (e.Row.Cells[appointmentIndex].Text == "0") e.Row.Cells[dateIndex].Text = "";
        //}
        //e.Row.Cells[appointmentIndex].Visible = false;
        
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    if (e.Row.Cells[13].Text == "1")
        //    {
        //        e.Row.BackColor = System.Drawing.Color.GhostWhite;
        //    }
        //}
        //e.Row.Cells[attended].Visible = false;
    }
    protected void dplActionMedical_SelectedIndexChanged(object sender, EventArgs e)
    {
        string MedicalAction = dplActionMedical.SelectedValue;
        if(MedicalAction != "" || MedicalAction != null)
        {
            txtNumberAction.Text = MedicalAction;
            txtDiagnostic.Focus();
        } 
    }
}