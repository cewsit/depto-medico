﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _NewConsultation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txtClocknumber.Attributes.Add("Autocomplete", "off");
        txtNameEmployee.Attributes.Add("Autocomplete", "off");
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("EmployeeMedicalInformation.aspx");
    }
    
}