﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Main.aspx.cs" Inherits="_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="content">
        <div class="row">
            <div class="col-md-6 col-md-offset-1">
                <div style="display: normal;" runat="server" id="historyCostsChart">
                    <div class="box box-widget" style="font-size: 12px;">
                        <div class="box-header">
                            <b>Tipos de Consultas </b><small>(Al mes)</small>
                        </div>
                        <div class="box-body" style="height: 300px;">
                            <span id="lblConsultationTags" runat="server"></span>
                            <span id="lblDataConsultation" runat="server"></span>
                            <canvas id="topConsultations" style="max-height: 290px; width: 100%"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="col-md-6">
                    <div class="info-box" style="zoom: 0.8;">
                        <span class="info-box-icon bg-gray"><i class="fa fa-hospital-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text text-center">Consultas Mes</span>
                            <span class="info-box-number" style="font-size: 40px; text-align: center" id="lblMonthlyConsultation" runat="server">-<small> </small></span>
                        </div>
                    </div>
                </div>
            <%--</div>
            <div class="col-md-3">--%>
                <div class="col-md-6">
                    <div class="info-box" style="zoom: 0.8;">
                        <span class="info-box-icon bg-gray"><i class="fa fa-medkit"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text text-center">Consultas dia</span>
                            <span class="info-box-number" style="font-size: 40px; text-align: center" id="lblDailyConsultations" runat="server">-<small> </small></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                 <div style="margin-top: 70px">
                    <div class="row">
                        <div class="col-md-8" style="margin-left:15px;">
                            <asp:LinkButton ID="btnFilterConsultation" runat="server" CssClass="btn btn-default form-control" Style="border-radius: 15px; color: black; font-weight: bold; background-color: #d2d6de !important;" OnClick="btnFilterConsultation_Click"><i class="fa fa-search" style="font-weight:bold">&nbsp;&nbsp;&nbsp; Consultas generales</i></asp:LinkButton>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-md-8" style="margin-left:15px;">
                            <asp:Button runat="server" ID="btnAddConsultation" CssClass="btn btn-default-lg form-control" Style="border-radius: 15px; background-color: #3c8dbc; color: white" Text="Nueva consulta" OnClick="btnAddConsultation_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="font-weight: bold; position: relative">CONSULTAS REALIZADAS</div>
                            <div class="panel-body" style="height: 800px; overflow-x: auto">
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:GridView runat="server" ID="GdvConfirmConsultation" Visible="true" CssClass="table table-responsive text-center sorting fordataTable" GridLines="None" OnRowDataBound="GdvConfirmConsultation_RowDataBound"></asp:GridView>
                                        <div class="form-group col-md-12" style="text-align: center; margin-top: 50px">
                                            <p id="pLogsConsultation" runat="server"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <%-- <div class="col-md-6">
                                <div class="info-box" style="zoom:0.8;">
                                    <span class="info-box-icon bg-gray"><i class="fa fa-hospital-o"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text text-center">Consultas Mes</span>
                                        <span class="info-box-number" style="font-size:40px;text-align:center" id="lblMonthlyConsultation" runat="server">-<small> </small></span>
                                    </div>
                                </div>
                            </div>--%>
                     <%--       <div class="col-md-6">
                                <div class="info-box" style="zoom: 0.8;">
                                    <span class="info-box-icon bg-gray"><i class="fa fa-medkit"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text text-center">Consultas dia</span>
                                        <span class="info-box-number" style="font-size: 40px; text-align: center" id="lblDailyConsultations" runat="server">-<small> </small></span>
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-3" style="display: none">
                        <div class="box box-widget">
                            <div class="box-header with-border">
                                <h3 class="box-title" style="font-weight: bold">Tipos de consultas<small>&nbsp;&nbsp;(Mes actual)</small></h3>
                                <div class="box-tools pull-right">
                                    <%--<button type="button" class="btn btn-box-tool"><i class="fa fa-minus"></i>
                                </button>--%>
                                </div>
                            </div>
                            <div class="box-body">
                                <div id="donut-chart" style="height: 280px;"></div>
                            </div>
                        </div>
                        <span id="donutData" runat="server" style="display: none;"></span>
                    </div>
                   
                </div>
            </div>
            <%--<div class="panel panel-default">
                    <div class="panel-heading" style="font-weight:bold">CITAS PROXIMAS</div>
                    <div class="panel-body" style="height:450px;overflow-x:auto">
                         
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView runat="server" ID="GdvCloseAppointments" Visible="true" CssClass="table table-responsive text-center sorting" GridLines="None" OnRowDataBound="GdvCloseAppointments_RowDataBound">
                                </asp:GridView>
                                <div class="form-group col-md-12" style="text-align:center;margin-top:50px"><p id="pLogsAppoinments" runat="server"></p></div>
                            </div>
                        </div>
                    </div>
                </div>--%>
            <%--</div>--%>
        </div>
    </div>
    <div class="modal" id="mdlDelete" runat="server" tabindex="-1" role="dialog" aria-labelledby="mdlDelete" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="border-radius: 10px">
                <div class="model-header">
                    <p class="modal-title" id="mdlDeleteLabel" style="margin-top: 300px; text-align: center"><b>ALERTA</b></p>
                </div>
                <div class="modal-body" style="text-align: center">
                    <p>¿Desea eliminar el registro?</p>
                </div>
                <div class="modal-footer">
                    <asp:LinkButton runat="server" ID="btnCloseModalDelete" CssClass="col-md-2 col-md-offset-5 col-md-pull-2 btn btn-default text-black" OnClick="btnCloseModalDelete_Click" ToolTip="Eliminar"><i class="fa fa-close"></i>Cerrar</asp:LinkButton>
                    <asp:LinkButton runat="server" ID="btnConfirmDelete" CssClass="col-md-2 btn btn-default text-red" OnClick="btnConfirmDelete_Click"><i class="fa fa-trash"></i>Eliminar</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="mdlAttended" runat="server" tabindex="-1" role="dialog" aria-labelledby="mdlAttended" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="border-radius: 10px">
                <div class="model-header">
                    <p class="modal-title" id="mdlAttendedLabel" style="margin-top: 300px; text-align: center"><b>ALERTA</b></p>
                </div>
                <div class="modal-body" style="text-align: center">
                    <p>Confirmando asistencia</p>
                </div>
                <div class="modal-footer">
                    <asp:LinkButton runat="server" ID="btnAttended" CssClass="col-md-2 col-md-offset-7 col-md-pull-2 btn btn-success text-white" OnClick="btnAttended_Click">Aceptar</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="mdldiagnostic" runat="server" tabindex="-1" role="dialog" aria-labelledby="mdlAttended" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="border-radius: 10px">
                <div class="model-header">
                    <p class="modal-title" id="mdldiagnosticlbl" style="margin-top: 300px; text-align: center"><b>Diagnóstico</b></p>
                </div>
                <br />
                <div class="modal-body" style="text-align: center">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <asp:Label runat="server" ID="lbldiagnostic" Style="text-transform: uppercase"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:LinkButton runat="server" ID="btnCloseDiagnostic" CssClass="col-md-2 col-md-offset-7 col-md-pull-2 btn btn-default" OnClick="btnCloseDiagnostic_Click">Cerrar</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

</asp:Content>



