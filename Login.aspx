﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
   
 <!-- Top content -->
    <div class="top-content">
        <div class="inner-bg">    
            <%--<div class="" style="position:absolute">
                <a href="http://192.168.144.10"><span><img src="assets/img/adduser.png" style="width:65px; margin:20px" /><b> Registrarse...</b></span></a>
            </div>--%>
            <div class="content">
                <div class="row" style="position: absolute;width: 1000px;">
                    <div class="col-md-6" id="divAlerts" runat="server">
                      <%--  <div class="callout callout-danger pad">
                            <h4>I am a danger callout!</h4>
                            <p>There is a problem that we need to fix. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                         </div>
                        <div class="callout callout-info pad">
                            <h4>I am a danger callout!</h4>
                            <p>There is a problem that we need to fix. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                         </div>--%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-md-offset-2 text-center">
                        <br />
                        <br />
                        <h4>Bienvenido(a) a </h4>
                        <h1><strong>DEPARTAMENTO MEDICO</strong> | Login </h1>
                        <h4>Por favor, introduzca su usuario y contraseña</h4>
                        <br />
                        <br />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 form-box" style="border-style: none; background-color:white;background-color:rgba(255,255,255,.5);border-radius:20px;">
                        <div class="form-top">
                            <div class="form-top-left" style="text-align:center;margin-top:40px">
                                <br />
                                <img class="profile-img-card" src="assets/img/LogoCEWS.png" />
                                <br />
                            </div>
                            <br />
                        </div>
                         <div class="form-bottom">
                            <div class="login-form">
                                <div class="col-md-10 col-md-offset-1 form-group" style="margin-top:30px;">
	                                <label class="sr-only" for="form-username">Username</label>
	                                <asp:TextBox ID="txtUser" name="form-username" placeholder="Usuario" class="form-username form-control" runat="server" OnTextChanged="txtUser_TextChanged" style="border-radius:10px;border-color:none !important;" > </asp:TextBox>                    
                                </div>
                                <div class="col-md-10 col-md-offset-1 form-group">
	                                <label class="sr-only" for="form-password">Password</label>
	                                <asp:TextBox ID="txtPassword" name="form-password" placeholder="Contraseña" class="form-password form-control" runat="server" style="border-radius:10px;border-color:none !important;"></asp:TextBox>
                                </div>
                                <br />
                                <%--<div class="row">
                                    <div class="col-md-6">
                                        <asp:Button ID="btnAccess" class="btn btn-primary btn-block" runat="server" Text="Acceder"  onclick="validate_User" />
                                    </div>
                                    <div class="col-md-6">
                                        <asp:Button ID="btnRegister" class="btn btn-success btn-block" runat="server" Text="Registrate"/>
                                    </div>
                                </div>--%>
                               
                                <div class="col-md-10 col-md-offset-1" style="margin-top:30px;"><asp:Button ID="btnAccess" class="btn btn-primary btn-block" runat="server" Text="Acceder"  onclick="validate_User" style="border-radius:10px" /></div>
                                 <br />
                                <div class="col-md-10 col-md-offset-1" style="margin-top:15px;margin-bottom:60px"><a href="http://192.168.144.10:89/New_User_Request.aspx?systemID=1" id="btnRegister" class="btn bg-aqua btn-block" style="border-radius:10px">Registrate</a></div>
                            </div>
                        </div>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<asp:Label ID="Label2" runat="server" Visible="False"></asp:Label>

    <div id="myModal" runat="server" class="modal fade in" style="display:none;margin-top:-15px;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          
        <div class="modal-content">
            <div class="modal-body">
                <asp:LinkButton ID="btnCloseModal" OnClick="btnCloseModal_Click" style="margin-top:-15px;margin-right:-12px;" runat="server" class=" pull-right" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa fa-close"></i> Cerrar</asp:LinkButton>        
                <img src="assets/img/notification.jpg" class="img-responsive">
            </div>
        </div>
      </div>
    </div>
</asp:Content>