﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    data sqlquery = new data();
    string DomainName = HttpContext.Current.Request.Url.ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["username"] != null)
        {
            Session["userid"] = Request.Cookies["userid"].Value;
            Session["username"] = Request.Cookies["username"].Value;
            Session["userpassword"] = Request.Cookies["password"].Value;
            Session["usertype"] = Request.Cookies["usertype"].Value;
            Session["expired_date"] = Request.Cookies["expired_date"].Value;
            Session["is_online"] = Request.Cookies["is_online"].Value;
            Session["clock"] = Request.Cookies["clock"].Value;

            Session["url_photos"] = Request.Cookies["url_photos"].Value;
            Session["url_sys_mgm"] = Request.Cookies["url_sys_mgm"].Value;
            Session["cttn_url_photos"] = Request.Cookies["cttn_url_photos"].Value;

            if (Request.UrlReferrer != null)
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                Response.Redirect("Main.aspx");
            }
        }

        txtPassword.TextMode = TextBoxMode.Password;
        txtUser.Focus();
    }
    protected void validate_User(object sender, EventArgs e)
    {
        string username = txtUser.Text.Replace("'", "").Trim();
        string Password = txtPassword.Text.Replace("'", "").Trim();
        if (username != "")
        {
            DataTable dt2 = sqlquery.validateUser(username);
            foreach (DataRow row in dt2.Rows)
            {
                Session["userid"] = row[0].ToString().Trim();
                Session["username"] = row[1].ToString().Trim();
                Session["userpassword"] = row[2].ToString().Trim();
                Session["usertype"] = "user";
                Session["expired_date"] = row[3].ToString().Trim();
                Session["is_online"] = row[4].ToString().Trim();
                Session["clock"] = row[5].ToString().Trim();
            }

            /*Get System Management Settings*/
            Session["url_photos"] = sqlquery.getStringValueSM("SELECT value from system_settings WHERE name='url_photos'; ");
            Session["url_sys_mgm"] = sqlquery.getStringValueSM("SELECT value from system_settings WHERE name='url_sys_mgm'; ");
            Session["cttn_url_photos"] = sqlquery.getStringValueSM("SELECT value FROM system_settings WHERE name='cttn_url_photos'");
            if (dt2.Rows.Count > 0) // Hubo resultados en la validacion
            {
                if (Session["userpassword"].ToString() == Password)
                {
                    sqlquery.newSystemLog("LOGIN", Session["userid"].ToString(), Session["username"].ToString(), "medical_department", DomainName, "Nuevo inicio de sesion correcto","info");

                    if(Session["userpassword"].ToString() == sqlquery.getStringValueSM("SELECT value from system_settings WHERE name='default_password'; "))
                    {
                        Session["is_online"] = null;
                        Response.Redirect(Session["url_sys_mgm"].ToString().Trim()+"change_password.aspx?url="+ DomainName+"&clock="+Session["clock"].ToString().Trim()); // CHANGE PASSWORD
                    }
                    else
                    {
                        /* Registro de Cookies */

                        if (Session["userid"] != null)
                        {
                            HttpCookie userid = new HttpCookie("userid", Session["userid"].ToString());
                            userid.Expires = DateTime.Now.AddHours(5);
                            Response.SetCookie(userid);
                        }

                        if (Session["username"] != null)
                        {
                            HttpCookie UserCookie = new HttpCookie("username", Session["username"].ToString());
                            UserCookie.Expires = DateTime.Now.AddHours(5);
                            Response.SetCookie(UserCookie);
                        }
                        if (Session["userpassword"] != null)
                        {
                            HttpCookie PassCookie = new HttpCookie("password", Session["userpassword"].ToString());
                            PassCookie.Expires = DateTime.Now.AddHours(5);
                            Response.SetCookie(PassCookie);
                        }
                        if (Session["usertype"] != null)
                        {
                            HttpCookie usertype = new HttpCookie("usertype", Session["usertype"].ToString());
                            usertype.Expires = DateTime.Now.AddHours(5);
                            Response.SetCookie(usertype);
                        }
                        if (Session["is_online"] != null)
                        {
                            HttpCookie is_online = new HttpCookie("is_online", Session["is_online"].ToString());
                            is_online.Expires = DateTime.Now.AddHours(5);
                            Response.SetCookie(is_online);
                        }
                        if (Session["clock"] != null)
                        {
                            HttpCookie clock = new HttpCookie("clock", Session["clock"].ToString());
                            clock.Expires = DateTime.Now.AddHours(5);
                            Response.SetCookie(clock);
                        }
                        if (Session["expired_date"] != null)
                        {
                            HttpCookie expired_date = new HttpCookie("expired_date", Session["expired_date"].ToString());
                            expired_date.Expires = DateTime.Now.AddHours(5);
                            Response.SetCookie(expired_date);
                        }
                        if (Session["url_photos"] != null)
                        {
                            HttpCookie urlPhotosCookie = new HttpCookie("url_photos", Session["url_photos"].ToString());
                            urlPhotosCookie.Expires = DateTime.Now.AddHours(5);
                            Response.SetCookie(urlPhotosCookie);
                        }
                        if (Session["url_sys_mgm"] != null)
                        {
                            HttpCookie urlSystemCookie = new HttpCookie("url_sys_mgm", Session["url_sys_mgm"].ToString());
                            urlSystemCookie.Expires = DateTime.Now.AddHours(5);
                            Response.SetCookie(urlSystemCookie);
                        }

                        if (Session["cttn_url_photos"] != null)
                        {
                            HttpCookie cttn_url_photos = new HttpCookie("cttn_url_photos", Session["cttn_url_photos"].ToString());
                            cttn_url_photos.Expires = DateTime.Now.AddHours(5);
                            Response.SetCookie(cttn_url_photos);
                        }

                        if (Request.Params["lasturl"] != null) 
                        {
                            Response.Redirect(Request.Params["lasturl"].ToString());
                        }
                        else
                        {
                            Response.Redirect("Main.aspx"); // Default Page

                        }
                    }
                }
                else
                {
                    Session["is_online"] = null;
                    newAlert("Contraseña Incorrecta", "danger", "");
                    txtPassword.Focus();
                }
            }
            else
            {
                newAlert("Usuario incorrecto<br/>Verifique los siguientes puntos:<br/> *Password Incorrecta <br/> *Fecha de expiracion", "danger","");
                txtUser.Focus();
            }
        }
    }

    protected void newAlert(string msj, string type,string title)
    {
        if (type == "") type = "info";
        if (title == "") title = "Alerta";

        string alert = "<div class=\"callout callout-"+type+" pad \">" +
                            "<h4> "+title+"!</h4>" +
                             "<p>" + msj + "</p>" +
                        "</div>";
        divAlerts.InnerHtml = alert;
    }



    protected void btnCloseModal_Click(object sender, EventArgs e)
    {
        myModal.Attributes.Add("style", "display:none;");
    }

    protected void txtUser_TextChanged(object sender, EventArgs e)
    {

    }
}