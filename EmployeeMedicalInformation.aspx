﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="EmployeeMedicalInformation.aspx.cs" Inherits="_EmployeeMedicalInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading" style="font-weight: bold">NUMERO DE RELOJ</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-push-1 input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </span>
                                <asp:TextBox name="txtclocknumber" type="number" runat="server" class="form-control" Style="width: 50%" ID="txtUserclocknumber"></asp:TextBox>
                                <div class="col-md-3">
                                    <asp:Button runat="server" ID="btnclocknumber" CssClass="btn btn-default btn-block btn-rounded btn-fw" Style="width: 130px; border-radius: 10px" Text="Mostrar" OnClick="btnclocknumber_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="font-weight: bold">EMPLEADO</div>
                    <div class="panel-body" style="height: 230px">
                        <div class="col-md-6 col-md-offset-3">
                            <img src="assets/img/avatar2.png" class="profile-user-img img-responsive" style="width: 100px; border: none" id="imageEmployee" runat="server" alt="" />
                            <br />
                        </div>
                        <div class="col-md-12">
                            <asp:Label ID="lblclocknumber" runat="server" CssClass="col-md-12 pull-right" Style="color: black; background-color: white; text-align: center; border: none; font: bold"></asp:Label>
                        </div>
                        <div class="col-md-12">
                            <asp:Label ID="lblNameEmployee" runat="server" CssClass="col-md-12 pull-right" Style="text-align: center; border: none"></asp:Label>
                        </div>
                        <p id="lblLogsnouser" runat="server" style="text-align: center" class="col-md-12"></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-default box-solid">
                    <div class="box-header with-border" style="background-color: #f5f5f5">
                        <p><b>CONSULTA MEDICA</b></p>
                    </div>
                    <div class="box-body" style="height: 347px">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-offset-2">
                                    <asp:Label runat="server" ID="lblActionMedical">Incidencia</asp:Label>
                                </div>
                                <div class="form-group col-md-3 col-md-offset-2">
                                    <asp:TextBox runat="server" ID="txtNumberAction" CssClass="form-control col-md-2" Style="border-radius: 15px" TextMode="Number" OnTextChanged="txtNumberAction_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    <p id="lblLogsno" runat="server"></p>
                                </div>
                                <div class="form-group col-md-6">
                                    <asp:DropDownList runat="server" ID="dplActionMedical" CssClass="form-control" Style="border-radius: 15px" AutoPostBack="true" OnSelectedIndexChanged="dplActionMedical_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-9 col-md-offset-2">
                                    <asp:Label runat="server" ID="lblDiagnostic">Diagnóstico</asp:Label><br />
                                    <asp:TextBox runat="server" ID="txtDiagnostic" CssClass="col-md-7 form-control" Style="border-radius: 15px"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-9 col-md-offset-2">
                                    <asp:Label runat="server" ID="lblMedicalTreatment">Tratamiento</asp:Label><br />
                                    <asp:TextBox runat="server" ID="txtMedicalTreatment" CssClass="col-md-7 form-control" Style="border-radius: 15px"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-11 col-md-offset-0">
                                    <div class="col-md-10">
                                        <asp:Label runat="server" ID="Label1">Nota</asp:Label><br />
                                        <asp:TextBox runat="server" TextMode="MultiLine" ID="txtConsultationNote" CssClass="col-md-7 form-control" Style="height: 85px; border-radius: 15px"></asp:TextBox>
                                    </div>
                                    <div class="col-md-1">
                                        <p id="lblLogsSaveConsultation" runat="server"></p>
                                        <br />
                                        <asp:Button runat="server" ID="btnReturnMain" CssClass=" btn btn-default form-control" Style="width: 150px; margin-bottom: 10px; border-radius: 25px" Text="Salir" OnClick="btnReturnMain_Click" />
                                        <asp:Button runat="server" ID="btnSaveInformation" CssClass=" btn btn-default form-control" Style="width: 150px; border-radius: 15px; background: #3c8dbc; color: white" Text="Guardar" Visible="true" OnClick="btnSaveInformation_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default active">
                    <div class="panel-heading" style="font-weight: bold">SIGNOS VITALES</div>
                    <div class="panel-body" style="height: 358px">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col" class="col-md-5"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">
                                        <img src="assets/img/glucosa.svg" alt="height" height="25px" width="25px" />&nbsp;&nbsp;&nbsp;<asp:Label ID="lblHeight" runat="server">Glucosa</asp:Label>
                                        <%--masa corporal por glucosa--%>                  
                                    </th>
                                    <td>
                                        <asp:TextBox ID="txtHeight" TextMode="Number" runat="server" CssClass="col-md-7 form-control" Style="border-radius: 15px"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <img src="assets/img/weight.svg" alt="weight" height="25px" width="25px" />&nbsp;&nbsp;&nbsp;<asp:Label ID="lblweight" runat="server">Peso</asp:Label></th>
                                    <td>
                                        <asp:TextBox ID="txtWeight" TextMode="Number" runat="server" CssClass="col-md-7 form-control" Style="border-radius: 15px"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <img src="assets/img/temperature.svg" alt="temperature" height="25px" width="25px" />&nbsp;&nbsp;&nbsp;<asp:Label ID="lbltemperature" runat="server">Temperatura (°C)</asp:Label>
                                    </th>
                                    <td>
                                        <asp:TextBox ID="txtTemperature" runat="server" CssClass="col-md-7 form-control" Style="border-radius: 15px"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <img src="assets/img/heart_rate.svg" alt="heart" height="25px" width="25px" />&nbsp;&nbsp;&nbsp;<asp:Label ID="lblheartrate" runat="server">Frecuencia Cardiaca</asp:Label></th>
                                    <td>
                                        <asp:TextBox ID="txtHeartRate" TextMode="Number" runat="server" CssClass="col-md-7 form-control" Style="border-radius: 15px"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        <img src="assets/img/blood-pressure.svg" alt="body" height="25px" width="25px" />&nbsp;&nbsp;&nbsp;<asp:Label ID="lblbodymass" runat="server">Presión arterial</asp:Label></th>
                                    <td><%--estatura por presion arterial--%>
                                        <asp:TextBox ID="txtBodyMass" runat="server" CssClass="col-md-7 form-control" Style="border-radius: 15px"></asp:TextBox></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#Consultation" data-toggle="tab" aria-expanded="true" runat="server">Consultas</a></li>
                        <li id="mDisability"><a href="#Disability" data-toggle="tab" aria-expanded="true" runat="server">Incapacidades</a></li>
                    </ul>
                    <div class="tab-content" style="height: 345px; overflow-x: auto;">
                        <div class="tab-pane active" id="Consultation">
                            <div class="box">
                                <div class="box-body">
                                    <asp:GridView runat="server" ID="GdvConsultationRegister" Visible="true" CssClass="table table-responsive table-condensed" GridLines="None" EmptyDataText="No cuenta con ningun registro"></asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="Disability">
                            <div class="box" style="height: auto; overflow-x: auto; width: auto">
                                <div class="box-body">
                                    <asp:GridView runat="server" ID="GdvMedicalDisability" Visible="true" CssClass="table table-responsive" GridLines="None" EmptyDataText="No cuenta con ningun registro"></asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
