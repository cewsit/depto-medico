﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _generalRegister : System.Web.UI.Page
{
    data SqlQuery = new data();
    DataTable users = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["is_online"] != null)
        {
            if (!IsPostBack)
            {
                getUsers();
            }

            getConsultationsByDay();
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        ComposeTablegeneralconsultation();
    }

    protected void getConsultationsByDay()
    {
        DataTable table = SqlQuery.getDatatablebyQueryHC("SELECT consultation_date 'Fecha','' 'Foto',CONDAY.clock_number 'No.reloj',CONDAY.name_employee 'Nombre',CONDAY.sickness 'Acción medica',CONDAY.diagnostic 'Diagnóstico',CONDAY.medicament 'Medicamento',CONDAY.user_id 'Usuario' FROM medical_consultation CONDAY WHERE YEAR(consultation_date) = YEAR(GETDATE()) AND MONTH(consultation_date) = MONTH(GETDATE()) ORDER BY consultation_date DESC");
        GdvRegisters.DataSource = table;
        GdvRegisters.DataBind();
    }
    protected void ComposeTablegeneralconsultation()
    {
        if (GdvRegisters.Rows.Count > 0)
        { 
            GdvRegisters.UseAccessibleHeader = true;
            GdvRegisters.HeaderRow.TableSection = TableRowSection.TableHeader;
            GdvRegisters.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
    protected void getUsers()
    {
        users = SqlQuery.getDatatablebyQuerySM("SELECT id_ua, username_ua FROM user_access");
    }
    protected string getUserNameById(string id)
    {
        string user_name = "";
        foreach(DataRow row in users.Rows)
        {
            if(row[0].ToString().Trim() == id)
            {
                user_name = row[1].ToString();
            }
        }
        //user_name = SqlQuery.getStringValueSM("SELECT username_ua FROM user_access WHERE id_ua =" + id + "");
        return user_name;
    }
    protected string SentenceCase(string Sentence)
    {
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        Sentence = textInfo.ToTitleCase(Sentence.ToLower());
        return Sentence;
    }

    protected void GdvRegisters_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int name = 7;
        int img = 1;
        int clock = 2;
        int name_employee = 3;
        int diagnostic = 5;
        int medicament = 6;

        if (e.Row.RowType != DataControlRowType.Header)
        {
            string image = "<img style=\"height:50px\" class=\"img-thumbnail zoom\" src=\"" + Session["url_photos"].ToString() + e.Row.Cells[clock].Text + ".jpg\"/>";

            e.Row.Cells[img].Text = image;

            e.Row.Cells[name].Text = getUserNameById(e.Row.Cells[name].Text);

            e.Row.Cells[name_employee].Text = SentenceCase(e.Row.Cells[name_employee].Text);

            if(e.Row.Cells[diagnostic].Text.Length > 30)
            {
                e.Row.Cells[diagnostic].Text = "<span title=\""+ e.Row.Cells[diagnostic].Text + "\">" + e.Row.Cells[diagnostic].Text.Substring(0, 30) + "</span>";
            }

            if (e.Row.Cells[medicament].Text.Length > 30)
            {
                e.Row.Cells[medicament].Text = "<span title=\"" + e.Row.Cells[medicament].Text + "\">" + e.Row.Cells[medicament].Text.Substring(0, 30) + "</span>";
            }
        }
    }
    protected void btnCloseCoonsultation_Click(object sender, EventArgs e)
    {
        Response.Redirect("Main.aspx");
    }

    protected void btnFilterConsultation_Click(object sender, EventArgs e)
    {
        logs.InnerHtml = "";

        string query = "SELECT consultation_date 'Fecha','' 'Foto',clock_number 'No.reloj',name_employee 'Nombre',sickness 'Acción medica',diagnostic 'Diagnóstico',medicament 'Medicamento',user_id 'Usuario' FROM medical_consultation WHERE consultation_date  BETWEEN '" + txtDateFilter1.Text + "' AND '" + txtDateFilter2.Text + "' ORDER BY consultation_date DESC";

        DataTable Result = SqlQuery.getDatatablebyQueryHC(query);
        if ( Result.Rows.Count <= 1)
        {
            logs.InnerHtml = "<span class=\"label bg-red\">No hay registros</span>";
        }
        else
        {
            GdvRegisters.DataSource = Result;
            GdvRegisters.DataBind();
        }
    }

}