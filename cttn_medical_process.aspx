﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="cttn_medical_process.aspx.cs" Inherits="_cttn_medical_process" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="content">
        <div class="row">
            <div class="col-md-6">
                <h4 class="content-header">Departamento Medico <small> Examen</small></h4>
            </div>
        </div>
        <div class="row" style="margin-top:30px;">
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-widget">
                            <div class="box-header">
                                <p class=""><b> En espera de proceso</b></p>
                            </div>
                            <div class="box-body" style="height:500px; overflow-x:auto;">
                                <asp:GridView ID="gdvMedicalPending" OnRowDataBound="gdvMedicalPending_RowDataBound" GridLines="None" AutoGenerateSelectButton="true" OnSelectedIndexChanged="gdvMedicalPending_SelectedIndexChanged" CssClass="table table-responsive table-striped fordataTable" runat="server">
                                    <selectedrowstyle backcolor="LightCyan" forecolor="DarkBlue" font-bold="true"/>  
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="box box-widget">
                    <div class="box-body" style="height:550px;">
                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-3 text-center">
                                <img src="assets/img/avatar2.png" alt="recruitment" style="width:110px;max-height:150px;" id="MTRecruitedImage" runat="server" class="img-rounded"/>
                            </div>
                            <div class="col-md-5">
                                <h5 style="margin-top:15px;"><b>Control: </b><span id="lblMTControlNumber" runat="server">-</span></h5>
                                <h5><b>Nombre: </b><span id="lblMTName" runat="server">-</span></h5>
                                <h5 style="margin-top:15px;"><b>Edad: </b><span id="lblMTAge" runat="server">-</span></h5>
                                <h5 style="margin-top:15px;"><b>Turno: </b><span id="lblMTShift" runat="server">-</span></h5>
                            </div>
                            <div class="col-md-4">
                                <h5 style="margin-top:10px;"><b>Genero: </b><span id="lblMTGender" runat="server">-</span></h5>
                                <h5 style="margin-top:15px;"><b>NSS: </b><span id="lblMTNSS" runat="server">-</span></h5>
                                <h5 style="margin-top:15px;"><b>CURP: </b><span id="lblMTCURP" runat="server">-</span></h5>
                                <h5 style="margin-top:15px;"><b>Nacionalidad: </b><span id="lblMTNationality" runat="server">-</span></h5>

                            </div>
                        </div>
                        <%--<div class="row">
                            <div class="col-md-3 col-md-offset-1">
                                <asp:CheckBox Text="Doping Positivo" CssClass="CheckStyle" runat="server" ID="cbDoping" OnCheckedChanged="ValidateCheckBox" AutoPostBack="true"/>
                            </div>
                            <div class="col-md-4">
                                <asp:CheckBox Text="Enfermedad General" CssClass="CheckStyle" runat="server" ID="cbGeneral" OnCheckedChanged="ValidateCheckBox" AutoPostBack="true" />
                                <asp:CheckBox Text="Agudeza Visual Deficiente" ID="cbVisual" CssClass="CheckStyle" runat="server" OnCheckedChanged="ValidateCheckBox" AutoPostBack="true" />
                                <asp:CheckBox Text="Obesidad" CssClass="CheckStyle" ID="cbObesity" runat="server" OnCheckedChanged="ValidateCheckBox" AutoPostBack="true" />
                            </div>
                            <div class="col-md-4">
                                <asp:CheckBox Text="Paciente Psiquiatrico" CssClass="CheckStyle" ID="cbPsychiatric" runat="server" OnCheckedChanged="ValidateCheckBox" AutoPostBack="true" />
                                <asp:CheckBox Text="Otro no especificado" CssClass="CheckStyle" ID="cbNotSpecified" runat="server" OnCheckedChanged="ValidateCheckBox" AutoPostBack="true" />
                            </div>
                        </div>--%>

                        <div class="row" style="margin-top:50px;">
                            <div class="col-md-4 col-md-offset-1">
                                <h5><b>Excluyentes Medicas</b></h5>
                                <asp:TextBox runat="server" CssClass="form-control form-line" ID="txtReasons" OnTextChanged="txtReasons_TextChanged" AutoPostBack="true"/>
                            </div>
                            <div class="col-md-4 col-md-offset-1">
                                <h5><b>Observaciones</b></h5>
                                <asp:TextBox runat="server" CssClass="form-control form-line" ID="txtComments" MaxLength="110" />
                            </div>
                        </div>
                        <div class="row" style="margin-top:60px">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="col-md-4">
                                    <asp:LinkButton ID="btnNoApt"  Visible="true" runat="server" CssClass="btn btn-block bg-red" OnClick="btnNoApt_Click"><i class="fa fa-times"></i> No Apto</asp:LinkButton>
                                </div>
                                <div class="col-md-4">
                                    <asp:LinkButton ID="btnConditionated"  Visible="true" runat="server" CssClass="btn btn-block bg-gray" OnClick="btnConditionated_Click"><i class="fa fa-legal"></i> Apto Condicionado</asp:LinkButton>
                                </div>
                                <div class="col-md-4" style="margin-bottom:20px;" >
                                    <asp:LinkButton ID="btnApt" Visible="true" runat="server" CssClass="btn btn-block bg-green" OnClick="btnApt_Click"><i class="fa  fa-check"></i> Apto</asp:LinkButton>
                                </div>
                                <p id="lblLogs" runat="server"></p>
                            </div>
                        </div>
                    </div>
                    <div class="overlay" id="MedicalOverlay" runat="server">

                    </div>
                </div>
            </div> 
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="box box-widget">
                    <div class="box-header">
                        <p><b>Metricos del dia</b> | <asp:TextBox ID="txtMetricsDayDate" OnTextChanged="txtMetricsDayDate_TextChanged" AutoPostBack="true" runat="server" CssClass="form-line" type="date" ></asp:TextBox></p>
                    </div>
                    <div class="box-body">
                        <table id="tblEfficienceByDay" class="table table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>Turno</th>
                                    <th>Agencia</th>
                                    <th>Resultado</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody id="tblBodyEfficienceByDay" runat="server">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="box box-widget">
                    <div class="box-body">
                        <%--Filtrado--%>
                        <div class="" >                                    
                            <div class="col-md-2">
                                <h5><i class="fa fa-medkit text-black"></i> Control</h5>
                                <asp:TextBox ID="txtNSS" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>                            
                            <div class="col-md-2">
                                <h5><i class="fa  fa-arrow-right text-black"></i> Status</h5>
                                <asp:DropDownList ID="dplStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <h5 id="lblDate" runat="server"><i class="fa fa-calendar text-black"></i> Fecha</h5>
                                <asp:TextBox type="date" ID="txtDate" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-1" style="margin-bottom:20px">
                                <h5><br /></h5>
                                <asp:LinkButton type="date" ID="btnFilter" runat="server" OnClick="btnFilter_Click" CssClass="btn btn-primary"><i class="fa fa fa-search text-white"></i> Buscar</asp:LinkButton>
                            </div>
                        </div>
                        
                        <asp:GridView ID="gdvLastProcessed" GridLines="None" AutoGenerateSelectButton="true" OnRowDataBound="gdvLastProcessed_RowDataBound" OnSelectedIndexChanged="gdvLastProcessed_SelectedIndexChanged" CssClass="table table-responsive table-striped" runat="server" >
                            <selectedrowstyle backcolor="LightCyan" forecolor="DarkBlue" font-bold="true"/>  
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-4 col-md-offset-0">
                        <div class="box box-danger" style="border-top-color: #0099ff !important; ">
                            <div class="box-body text-center">
                                <div class="row">
                                    <div class="col-md-6 text-right">
                                         <i class="fa fa-users" style="font-size:25px;margin-top:15px;"></i>
                                    </div>
                                    <div class="col-md-6 text-left">
                                        <p style="font-size:40px" id="lblInRecruitment" runat="server">-</p>
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <small><b>En Contratacion</b></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-danger" style="border-top-color: #660066 !important; ">
                            <div class="box-body text-center">
                                <div class="row">
                                    <div class="col-md-6 text-right">
                                         <i class="fa fa-hourglass-half" style="font-size:25px;margin-top:15px;"></i>
                                    </div>
                                    <div class="col-md-6 text-left">
                                        <p style="font-size:40px" id="lblWaiting" runat="server">-</p>
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <small><b>En Espera</b></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-danger" style="border-top-color: #009933 !important; ">
                            <div class="box-body text-center">
                                 <div class="row">
                                    <div class="col-md-6 text-right">
                                         <i class="fa fa-check-circle" style="font-size:25px;margin-top:15px;"></i>
                                    </div>
                                    <div class="col-md-6 text-left">
                                        <p style="font-size:40px" id="lblProcessed" runat="server">-</p>                                                                        
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <small><b>Finalizados</b></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

<!-- Modal -->
    <div class="modal fade in" id="ConfirmResultModal" runat="server" tabindex="-1" role="dialog" aria-labelledby="ConfirmResultModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirmar resultado - <span id="lblModalControlNumber" runat="server"></span></h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <p class="bg-green img-rounded text-center" style="font-size:20px;" id="lblModalResult" runat="server"></p>
                        </div>
                    </div>
                    <div class="row" style="margin-top:20px;">
                        <div class="col-md-4 col-md-offset-2">
                            <p class="text-gray"><b>Excluyentes</b></p>
                            <p class="" id="lblModalReasons" runat="server">Sin Excluyentes</p>
                        </div>
                        <div class="col-md-4">
                            <p class="text-gray"><b>Observaciones</b></p>
                            <p id="lblModalComments" class="" runat="server">Sin Observaciones</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:LinkButton class="btn btn-default no-border" id="btnModalCancel" runat="server" OnClick="btnModalCancel_Click">Cancelar</asp:LinkButton>
                    <asp:LinkButton  class="btn btn-primary" id="btnModalConfirm" runat="server" OnClick="btnModalConfirm_Click">Confirmar</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

</asp:Content>