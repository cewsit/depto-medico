﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Main : System.Web.UI.Page
{
    data SqlQuery = new data();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["is_online"] != null)
        {
            if (!Page.IsPostBack)
            {
                getConsultationsTheDay();
                //getUpcomingMedicalAppointments();
            }
                GetMetrics();
            GetConsultationbyMonth();
            //donutData.Attributes.Add("data", GetConsultationbyMonth());

            if (Request.Params["mode"]!= null)
            { 
                if(Request.Params["mode"].ToString().Trim() == "delete")
                {
                    mdlDelete.Attributes.Add("style", "display:block;");
                }
                if (Request.Params["mode"].ToString().Trim() == "attended")
                {
                    mdlAttended.Attributes.Add("style", "display:block;");                                                    
                }
                if (Request.Params["mode"].ToString().Trim() == "diagnostic")
                {
                    mdldiagnostic.Attributes.Add("style", "display:block;");

                    lbldiagnostic.Text = Request.Params["id"].ToString();
                    lbldiagnostic.Text = SqlQuery.getStringValueHC("SELECT diagnostic FROM medical_consultation WHERE id = '"+ lbldiagnostic.Text +"'");
                    lbldiagnostic.Visible = true;
                    
                }
                
            }
            LogsConsultation();
            //LogsAppointments();
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    protected string GetConsultationbyMonth()
    {
        /// Aqui haces todo el modelo, Query, Tablas

        string innertext = "[";
        int count = 0;
        int others = 0;

        string[] colors = new string[5];

        colors[0] = ("#35A7E8");
        colors[1] = ("#9735E8");
        colors[2] = ("#D2E835");
        colors[3] = ("#EC6F89");
        colors[4] = ("#0073b7");

        DataTable table = SqlQuery.getDatatablebyQueryHC("SELECT COUNT(*) 'total', sickness FROM medical_consultation WHERE MONTH(consultation_date) = MONTH(GETDATE()) AND YEAR(consultation_date) = YEAR(GETDATE()) GROUP BY sickness ORDER BY 'total' DESC");      //SELECCIONA SOLO LOS PRIMEROS 5 SINTOMAS MAS CONSULTADOS
        
        string dataCount="[";
        string dataLabel = "[";
        foreach (DataRow row in table.Rows)
        {
            
            if(row["sickness"].ToString() != "")
            {
                dataCount += row["total"].ToString() + ",";
                dataLabel += "\"" + row["sickness"].ToString() + "\",";
            }

            //int sum = 0;

            //if(count < 5)
            //innertext += "{ 'label':'" + row["sickness"].ToString() + "', 'data': '" + row["total"].ToString() + "', 'color':'"+ colors[count] +"'},";

            //else
            //{
            //    int.TryParse(row["total"].ToString(), out sum);
            //    others += sum;
            //}

            //count++;
            

            //if (table.Rows.Count == count)
            //{
            //    innertext += "{ 'label':'otros', 'data': '" + others + "', 'color': '#FA3939' }] ";
            //}
        }
        dataCount = dataCount.TrimEnd(',') + "]";
        dataLabel = dataLabel.TrimEnd(',') + "]";
        lblConsultationTags.Attributes.Add("data", dataLabel);
        lblDataConsultation.Attributes.Add("data", dataCount);
        return innertext;
    }
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        ComposeTableConsultationsTheDay();
        //ComposeTableUpcomingMedicalAppointments();
    }
    protected void GetMetrics()
    {
        lblDailyConsultations.InnerHtml =  SqlQuery.getIntValueHC("SELECT count(*) FROM medical_consultation CONDAY WHERE CAST (consultation_date AS DATE) = CAST (GETDATE() AS DATE)").ToString();
        lblMonthlyConsultation.InnerHtml = SqlQuery.getIntValueHC("SELECT count(*) FROM medical_consultation CONDAY WHERE MONTH(consultation_date) = MONTH(GETDATE()) AND YEAR(consultation_date) = YEAR(GETDATE())").ToString();
    }
    protected void getConsultationsTheDay()
    {
        DataTable table = SqlQuery.getDatatablebyQueryHC("SELECT CONVERT(VARCHAR(10),consultation_date,108) 'Hora','' 'Foto',CONDAY.clock_number 'No.reloj',CONDAY.name_employee 'Nombre',CONDAY.sickness 'Acción medica',CONDAY.medicament 'Medicamento',CONDAY.diagnostic 'Diagnóstico',CONDAY.id FROM medical_consultation CONDAY WHERE CAST (consultation_date AS DATE) = CAST (GETDATE() AS DATE) ORDER BY consultation_date DESC");
        GdvConfirmConsultation.DataSource = table;
        GdvConfirmConsultation.DataBind();
    }
    protected void ComposeTableConsultationsTheDay()
    {
        if(GdvConfirmConsultation.Rows.Count > 0)
        {
            GdvConfirmConsultation.UseAccessibleHeader = true;
            GdvConfirmConsultation.HeaderRow.TableSection = TableRowSection.TableHeader;
            GdvConfirmConsultation.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
    //protected void getUpcomingMedicalAppointments()
    //{
    //    DataTable table = SqlQuery.getDatatablebyQueryHC("SELECT '-' 'Acciones',MEDAPP.id,MEDAPP.clock_number 'No.reloj', MEDAPP.new_consultation_date 'Fecha de cita',MEDAPP.reason_consultation 'Razón',MEDAPP.attended_appointment FROM medical_consultation MEDAPP WHERE is_appointment = 1 ORDER BY[new_consultation_date]");
    //    GdvCloseAppointments.DataSource = table;
    //    GdvCloseAppointments.DataBind();
    //}
    //protected void ComposeTableUpcomingMedicalAppointments()
    //{
    //    if(GdvCloseAppointments.Rows.Count > 0)
    //    {
    //        GdvCloseAppointments.UseAccessibleHeader = true;
    //        GdvCloseAppointments.HeaderRow.TableSection = TableRowSection.TableHeader;
    //        GdvCloseAppointments.FooterRow.TableSection = TableRowSection.TableFooter;
    //    }
    //}
    protected void GdvCloseAppointments_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int action = 0;
        int idrow = 1;
        int attendedd = 5;

        if (e.Row.RowType != DataControlRowType.Header)
        {
            string attended = "<a href=\"Main.aspx?&mode=attended&id=" + e.Row.Cells[idrow].Text + "\" class=\"text-blue\" style=\"margin-left:20px;\"><span class=\"fa fa-check-square\"></span></a>";
            string Delete = "<a href=\"Main.aspx?&mode=delete&id=" + e.Row.Cells[idrow].Text + "\" class=\"text-black\" style=\"margin-left:20px;\"><span class=\"fa fa-trash\"></span></a>";

            e.Row.Cells[action].Text = attended + Delete;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[5].Text == "1")
            {
                e.Row.BackColor = System.Drawing.Color.GhostWhite;
            }
        }
        e.Row.Cells[action].Attributes["width"] = "15%";
        e.Row.Cells[idrow].Visible = false;
        e.Row.Cells[attendedd].Visible = false;
    }
    protected string getUserNameById(string id)
    {
        string user_name = "";
        user_name = SqlQuery.getStringValueSM("SELECT username_ua FROM user_access WHERE id_ua =" + id + "");
        return user_name;
    }
    protected string SentenceCase(string Sentence)
    {
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        Sentence = textInfo.ToTitleCase(Sentence.ToLower());
        return Sentence;
    }
    protected void GdvConfirmConsultation_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int id = 7;
        //int name = 7;
        int img = 1;
        int clock = 2;
        int name_employee = 3;
        int medicament = 5;
        int diagnostic = 6;

        string image = "<img style=\"height:50px\" class=\"img-thumbnail zoom\" src=\"" + Session["url_photos"].ToString() + e.Row.Cells[clock].Text + ".jpg\"/>";
        //string icondiagnostic = "<a href=\"Main.aspx?&mode=diagnostic&id=" + e.Row.Cells[id].Text + "\" class=\"text-black\"><span class=\"fa  fa-stethoscope\" title=\"" + e.Row.Cells[diagnostic].Text + "\"></span></a>";

        if (e.Row.RowType != DataControlRowType.Header)
        {
            e.Row.Cells[img].Text = image;
            //e.Row.Cells[diagnostic].Text = icondiagnostic;
           
            //e.Row.Cells[name].Text = getUserNameById(e.Row.Cells[name].Text);

            e.Row.Cells[name_employee].Text = SqlQuery.getStringValueHC("SELECT Nombre FROM HEADCOUNT WHERE Numreloj="+ (e.Row.Cells[clock]).Text +"");

            e.Row.Cells[name_employee].Text = SentenceCase(e.Row.Cells[name_employee].Text);

            if (e.Row.Cells[medicament].Text.Length > 30)
            {
                e.Row.Cells[medicament].Text = "<span title=\"" + e.Row.Cells[medicament].Text + "\">" + e.Row.Cells[medicament].Text.Substring(0, 30) + "</span>";
            }
            if (e.Row.Cells[diagnostic].Text.Length > 30)
            {
                e.Row.Cells[diagnostic].Text = "<span title=\"" + e.Row.Cells[diagnostic].Text + "\">" + e.Row.Cells[diagnostic].Text.Substring(0, 30) + "</span>";
            }
        }
        e.Row.Cells[7].Visible = false;
    }
    protected void btnCloseModalDelete_Click(object sender, EventArgs e)
    {
        Response.Redirect("Main.aspx");
    }
    protected void btnAttended_Click(object sender, EventArgs e)
    {
        string id = Request.Params["id"].ToString();

        SqlQuery.getStringValueHC("UPDATE medical_consultation SET attended_appointment='1' WHERE id='"+ id +"'");
        Response.Redirect("Main.aspx");
    }
    protected void btnConfirmDelete_Click(object sender, EventArgs e)
    {
        string id = Request.Params["id"].ToString();
        string Query = "UPDATE medical_consultation SET new_consultation_date = NULL, reason_consultation='' WHERE id ='" + id + "'";// vacia campos a fin de que no se detecten en la consulta

        if (SqlQuery.getStringValueHC(Query) == "")
        {
            //getUpcomingMedicalAppointments();
        }
        else
        {
            Response.Redirect("Main.aspx");
        }
    }
    protected void btnAddConsultation_Click(object sender, EventArgs e)
    {
        Response.Redirect("EmployeeMedicalInformation.aspx");
    }
    protected void LogsConsultation()
    {
        if (GdvConfirmConsultation.Rows.Count == 0)
        {
              pLogsConsultation.InnerHtml = "<span class=\"#D3D3D3\" style=\"font-weight:bold;\"> No se han realizado consultas </span>";
        }
        else
        {
            getConsultationsTheDay();
        }
    }
    //protected void LogsAppointments()
    //{
    //    if (GdvCloseAppointments.Rows.Count == 0)
    //    {
    //        pLogsAppoinments.InnerHtml = "<span class=\"#D3D3D3\" style=\"font-weight:bold;\"> No existen citas en este mes </span>";
    //    }
    //    else
    //    {
    //        getUpcomingMedicalAppointments();
    //    }
    //}

    protected void btnCloseDiagnostic_Click(object sender, EventArgs e)
    {
        Response.Redirect("Main.aspx");
    }

    protected void btnFilterConsultation_Click(object sender, EventArgs e)
    {
        Response.Redirect("generalRegister.aspx");
    }
}

/*NOTA: ESTE CODIGO SE UTILIZA PARA CONVERTIR*/

//if (count >= 4)
//{

//}
//else
//{
//if(count >= 10)
//{
//    int total = 0;
//    int.TryParse(row["total"].ToString(), out total);
//    others += total;
//}
//}